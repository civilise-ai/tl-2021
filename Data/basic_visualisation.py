import numpy as np
import matplotlib.pyplot as plt
import os

if __name__ == '__main__':

    Z = np.loadtxt('araluen.txt')
    plt.imshow(Z, alpha=0)
    plt.contour(Z, levels=100)
    # plt.show()
    path = os.path.expanduser("~/Desktop/araluen.png")
    plt.savefig(path)


