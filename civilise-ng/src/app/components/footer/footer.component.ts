import { Component, OnInit } from '@angular/core';

interface ContactInfo {
  name: string;
  phone: string;
  email: string;
  location: string;
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  title: string;
  about: string;
  email: string;
  phone: string;
  gitlab: string;
  linkedIn: string;
  twitter: string;
  currentYear: number;

  john: ContactInfo = {
    name: "John Forbes",
    email: "John.Forbes@civilise.ai",
    phone: "+61 4 2046 0728",
    location: "Australia"
  }

  yasar: ContactInfo = {
    name: "Yasar Adeel",
    email: "Yasar.Adeel@civilise.ai",
    phone: "+91 9 7039 08495",
    location: "India"
  }

  constructor() {
    this.title = "Civilise.ai";
    this.about = "Civilise.ai makes sustainable and regenerative Australian farming easy and accessible, applying artificial intellegence to Geospatial data.";
    // this.gitlab = "https://gitlab.com/civilise-ai/tl-2021/";
    this.linkedIn = "https://www.linkedin.com/company/civiliseai/about/";
    this.twitter = "https://twitter.com/civiliseai";
    this.currentYear = new Date().getFullYear();
  }

  ngOnInit(): void {
  }

}
