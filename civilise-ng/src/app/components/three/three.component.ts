import {
  Component,
  Input,
  ElementRef,
  ViewChild,
  Output,
  EventEmitter,
  OnInit,
} from '@angular/core';
import { LayerService, Layer } from 'src/app/services/layers.service';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

@Component({
  selector: 'app-three',
  templateUrl: `./three.component.html`,
  styleUrls: ['./three.component.scss'],
})
export class ThreeComponent implements OnInit {
  @ViewChild('rendererContainer') rendererContainer: ElementRef;

  @Input() layerDem: any;
  aerialService: Layer = {} as Layer;
  cadastreService: Layer = {} as Layer;
  trenchesEvenService: Layer = {} as Layer;
  trenchesStepService: Layer = {} as Layer;
  gulliesService: Layer = {} as Layer;
  ridgesService: Layer = {} as Layer;
  @Input() layerPaddocks: any;
  @Input() terrainExaggeration: any;
  @Input() terrainExaggerationOn: any;

  // Paddock mouseover information - these send those to result.componenet.ts
  @Output() setPaddockID = new EventEmitter<any>();
  onSetPaddockID(paddockID) {
    this.setPaddockID.emit(paddockID);
  }

  @Output() setPaddockArea = new EventEmitter<any>();
  onSetPaddockArea(paddockArea) {
    this.setPaddockArea.emit(paddockArea);
  }

  @Output() setPaddockPerimeter = new EventEmitter<any>();
  onSetPaddockPerimeter(paddockPerimeter) {
    this.setPaddockPerimeter.emit(paddockPerimeter);
  }

  @Output() setPaddockNull = new EventEmitter<any>();
  onSetPaddockNull() {
    this.setPaddockNull.emit();
  }

  ////////// CONSTANTS feel free to tweak ////
  aspectRatio = 1; // Aspect ratio of the 3D viewer window
  screenScale = 1; // How big the 3D viewer window should be compared to the screen size
  landscapeHeightSize = 1.1; // Maximum on-screen height of landscape in three.js world units
  landscapeWidthSize = 6.0; // Maximum on-screen width or length of landscape in three.js world units
  ////////// END CONSTANTS ///////////////////

  // Scene and rendering variables
  renderer = new THREE.WebGLRenderer({ antialias: true });
  scene;
  camera;
  mesh;
  cube;
  controls;

  // Utility Variables
  width;
  length;
  minimumHeight;
  maximumHeight;
  heightScalingFactor;
  widthScalingFactor;

  // Mouseover detection stuff
  INTERSECTED;
  mouse;
  projector;
  highlightedPaddock = -1;

  // Continuity variables
  landscapeArrayCache = undefined; // Used to remember the landscape so we can add images to it without reloading the array externally
  baseImageCache = undefined; // Used to remember the initial image pasted on there so we know how to add or remove boolean map overlays
  booleanMaps = new Array(5).fill(undefined); // Used to store all of the boolean arrays ([n][x][y] of booleans where n is the index of the boolean map) as well as colour, opacity and visibility

  constructor(private layerService: LayerService) {
    this.scene = new THREE.Scene();
    this.aspectRatio = window.innerWidth / window.innerHeight;
    this.camera = new THREE.PerspectiveCamera(75, this.aspectRatio, 0.1, 1200);
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.update();
  }

  ngOnInit() {
    this.layerService.getAerial.subscribe((result) => {
      this.aerialService = result;
      this.updateModel();
    });
    this.layerService.getCadastre.subscribe((result) => {
      this.cadastreService = result;
      this.updateModel();
    });

    this.layerService.getTrenchesEven.subscribe((result) => {
      this.trenchesEvenService = result;
      this.updateModel();
    });
    this.layerService.getTrenchesStep.subscribe((result) => {
      this.trenchesStepService = result;
      this.updateModel();
    });
    this.layerService.getGullies.subscribe((result) => {
      this.gulliesService = result;
      this.updateModel();
    });
    this.layerService.getRidges.subscribe((result) => {
      this.ridgesService = result;
      this.updateModel();
    });
  }

  ngOnChanges() {
    // console.log('ngOnChanges()');
    //console.log(this.layerPaddocks.on);
    //console.log(this.layerPaddocks);

    // console.log("aaaaaaaAAAAAAaaaaaaaAAAAAAaaaaaaaAAAAAAaaaaaaaAAAAAAaaaaaaaAAAAAA");
    // console.log(this.trenchesEvenService.data);
    // console.log(this.trenchesStepService.data);
    // console.log(this.gulliesService.data);
    // console.log(this.ridgesService.data);

    // this.showLandscape(this.layerDem);
    // this.showDefaultTexture(undefined);
    // this.toggleSatellite();
    // this.getOverlays();
    // this.overlayBooleanMaps();
    this.updateModel(); // the contents of this function were here here in full before abstraction
    //this.rendererContainer.nativeElement.appendChild(this.renderer);
  }

  ngAfterViewInit() {
    this.configCamera();
    this.configRenderer();
    this.configControls();
    this.configScene();
    this.animate();
  }

  // TODO: review the activities of this function, is it unnecessary to do so much each time a layer is changed?
  updateModel() {
    this.showLandscape(this.layerDem);
    this.showDefaultTexture(undefined);
    this.toggleSatellite();
    this.getOverlays();
    this.overlayBooleanMaps();
  }

  configCamera() {
    // Position the camera in a downward view
    this.camera.position.z = 5;
    this.camera.position.y = 1.75;
    this.camera.rotation.x = -0.3;
  }

  configRenderer() {
    this.renderer.setPixelRatio(window.devicePixelRatio);
    //this.renderer.setSize(window.innerWidth * this.screenScale, window.innerHeight * this.screenScale / this.aspectRatio);
    this.renderer.setSize(
      window.innerWidth * this.screenScale,
      window.innerHeight * this.screenScale
    );
    //this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.renderer.setClearColor('#d6efff');
    this.renderer.domElement.style.display = 'block';
    this.renderer.domElement.style.margin = 'auto';
    this.rendererContainer.nativeElement.appendChild(this.renderer.domElement);
  }

  configScene() {
    let materialArray = [];
    let texture_sky = new THREE.TextureLoader().load('assets/sky.png');
    let texture_up = new THREE.TextureLoader().load('assets/top.png');
    let texture_dn = new THREE.TextureLoader().load('assets/bottom.png');

    materialArray.push(new THREE.MeshBasicMaterial({ map: texture_sky }));
    materialArray.push(new THREE.MeshBasicMaterial({ map: texture_sky }));
    materialArray.push(new THREE.MeshBasicMaterial({ map: texture_up }));
    materialArray.push(new THREE.MeshBasicMaterial({ map: texture_dn }));
    materialArray.push(new THREE.MeshBasicMaterial({ map: texture_sky }));
    materialArray.push(new THREE.MeshBasicMaterial({ map: texture_sky }));

    for (let i = 0; i < 6; i++) materialArray[i].side = THREE.BackSide;

    let skyboxGeo = new THREE.BoxGeometry(1000, 1000, 1000);
    let skybox = new THREE.Mesh(skyboxGeo, materialArray);
    this.scene.add(skybox);

    var amblight = new THREE.AmbientLight(0xffffff);
    this.scene.add(amblight);

    // Allow the window to be resized live
    window.addEventListener('resize', () => {
      // this.renderer.setSize(window.innerWidth * this.screenScale, window.innerHeight * this.screenScale / this.aspectRatio);
      this.renderer.setSize(
        window.innerWidth * this.screenScale,
        window.innerHeight * this.screenScale
      );
    });

    // Add mouse moved listener
    this.mouse = new THREE.Vector2();
    window.addEventListener('mousemove', (event) => {
      // update the mouse variable
      this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
      this.mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
      this.mouse.y -= 0.05; // There's this stupid annoying offset so instead of finding out where that's coming from ima just do this
      //this.mouse.x = (event.clientX / this.renderer.domElement.clientWidth) * 2 - 1;
      //this.mouse.y = - (event.clientY / this.renderer.domElement.clientHeight) * 2 + 1;
      //console.log(this.mouse.x);
    });
  }

  configControls() {
    this.controls.autoRotate = false;
    this.controls.enableZoom = true;
    this.controls.enablePan = true; // hold shift to pan
    this.controls.update();
  }

  /*
   * showLandscape
   * This function takes a 2D array of height values, and displays it as a mesh on the webpage
   * Can be called repeatedly for example as a simulation progresses.
   *
   */
  showLandscape = function (landscapeArray) {
    this.landscapeArrayCache = landscapeArray;
    this.scene.remove(this.landscapeMesh);
    this.landscapeMesh = this.generateLandscape(
      landscapeArray,
      this.baseImageCache
    );
    this.scene.add(this.landscapeMesh);
  };

  // Do not call this function externally, use showLandscape() instead.
  // Pass this function a 2D array of the height at every grid point in the landscape
  // Returns a landscape mesh that can be displayed
  generateLandscape = function (landscapeArray, colourArray) {
    // Default landscape
    if (landscapeArray == undefined) {
      landscapeArray = [
        [20, 25, 20, 19],
        [23, 23, 22, 17],
        [15, 10, 16, 17],
        [16, 12, 12, 17],
        [23, 23, 22, 13],
      ];
      this.landscapeArrayCache = landscapeArray;
    }

    // Gather metadata
    this.width = landscapeArray.length;
    this.length = landscapeArray[0].length;

    this.minimumHeight = 100000;
    this.maximumHeight = -100000;
    for (x = 0; x < this.width; x++) {
      for (z = 0; z < this.length; z++) {
        this.maximumHeight = Math.max(this.maximumHeight, landscapeArray[x][z]);
        this.minimumHeight = Math.min(this.minimumHeight, landscapeArray[x][z]);
      }
    }
    // For flatter properties let's not exagerate the scaling too much
    if (this.maximumHeight - this.minimumHeight < 25) {
      this.landscapeHeightSize = 0.5;
      // console.log("This is a fairly flat property. Reducing height scale to 0.5");
    }
    this.heightScalingFactor =
      this.landscapeHeightSize / (this.maximumHeight - this.minimumHeight);

    if (this.terrainExaggerationOn) {
      this.heightScalingFactor =
        this.heightScalingFactor * this.terrainExaggeration;
    }

    this.widthScalingFactor =
      this.landscapeWidthSize / Math.max(this.width, this.length);

    // Debug log the metadata
    // console.log("width " + this.width + " length " + this.length + " HSF " + this.heightScalingFactor + " WSF " + this.widthScalingFactor);

    var landscapeGeometry = new THREE.BufferGeometry();

    var indices = [];
    var vertices = [];
    var normals = [];
    var colors = [];

    // generate vertices, normals and color Data for a simple grid geometry
    for (var i = 0; i < this.width; i++) {
      for (var j = 0; j < this.length; j++) {
        var z =
          i * this.widthScalingFactor -
          this.width * this.widthScalingFactor * 0.5; // fix these
        var x =
          j * this.widthScalingFactor -
          this.length * this.widthScalingFactor * 0.5; //
        var y =
          (landscapeArray[i][j] - this.minimumHeight) *
          this.heightScalingFactor;

        vertices.push(x, y, z);
        normals.push(0, 0, 1);

        // Add colours to the mesh
        if (colourArray == undefined) {
          colors.push(1.0, 0.0, 1.0);
        } else {
          if (i < colourArray.length && j < colourArray[0].length) {
            // Using layer instead of this.cadastreService.data atm.
            /*if (this.cadastreService.on && this.cadastreService.data != null){
                if (i < this.cadastreService.data.length){
                    if (j < this.cadastreService.data[i].length){
                        if (!this.cadastreService.data[i][j]){
                            let grey = (colourArray[i][j][0]+colourArray[i][j][1]+colourArray[i][j][2])/3.0;
                            colourArray[i][j][0] += grey*0.3;
                            colourArray[i][j][1] += grey*0.3;
                            colourArray[i][j][2] += grey*0.3;
                            colourArray[i][j][0] *= 0.73;
                            colourArray[i][j][1] *= 0.73;
                            colourArray[i][j][2] *= 0.73;
                        }else{
                            colourArray[i][j][0] *= 1.05;
                            colourArray[i][j][1] *= 1.05;
                            colourArray[i][j][2] *= 1.05;
                        }
                    }
                }
            }*/
            colors.push(
              colourArray[i][j][0] / 255.0,
              colourArray[i][j][1] / 255.0,
              colourArray[i][j][2] / 255.0
            );
          } else {
            colors.push(1.0, 0.0, 1.0);
          }
        }
      }
    }

    // Generate triagnles
    for (var i = 0; i < this.width - 1; i++) {
      for (var j = 0; j < this.length - 1; j++) {
        var a = i * this.length + j;
        var b = i * this.length + j + 1;
        var c = (i + 1) * this.length + j;
        var d = (i + 1) * this.length + j + 1;

        // generate two faces (triangles) per iteration
        indices.push(a, b, c);
        indices.push(c, b, d);
      }
    }

    landscapeGeometry.setIndex(indices);
    landscapeGeometry.setAttribute(
      'position',
      new THREE.Float32BufferAttribute(vertices, 3)
    );
    landscapeGeometry.setAttribute(
      'normal',
      new THREE.Float32BufferAttribute(normals, 3)
    );
    landscapeGeometry.setAttribute(
      'color',
      new THREE.Float32BufferAttribute(colors, 3)
    );

    var material = new THREE.MeshStandardMaterial({
      side: THREE.DoubleSide,
      vertexColors: true,
    });

    var newObject = new THREE.Mesh(landscapeGeometry, material);
    newObject.name = 'landscape';
    return newObject;
  };

  /*
   * generateDefaultTexture
   * Generates a default texture image array and applies to the current loaded mesh
   * If an rgb map is given as an argument, this will be mixed in as well
   */
  showDefaultTexture = function (aerial_rgb) {
    if (this.landscapeArrayCache == undefined) {
      console.error(
        'showDefaultTexture: Tried to generate default texture for mesh when no mesh was loaded'
      );
      return;
    }

    var defaultColourArray = [];
    for (var i = 0; i < this.width; i++) {
      var arr2A = [];
      for (var j = 0; j < this.length; j++) {
        var y =
          (this.landscapeArrayCache[i][j] - this.minimumHeight) *
          this.heightScalingFactor; // Convert raw height Data to mesh Data (same equation used in generateLandscape())

        // Get heights of surrounding points so we can compare and set it's colour
        var y1,
          y2,
          y3,
          y4 = y;
        if (i < this.width - 1) {
          y1 =
            (this.landscapeArrayCache[i + 1][j] - this.minimumHeight) *
            this.heightScalingFactor;
        }
        if (i > 0) {
          y2 =
            (this.landscapeArrayCache[i - 1][j] - this.minimumHeight) *
            this.heightScalingFactor;
        }
        if (j < this.length - 1) {
          y3 =
            (this.landscapeArrayCache[i][j + 1] - this.minimumHeight) *
            this.heightScalingFactor;
        }
        if (j > 0) {
          y4 =
            (this.landscapeArrayCache[i][j - 1] - this.minimumHeight) *
            this.heightScalingFactor;
        }

        // Set Set how much of each colour cmyb we want based on height differences
        var colourC =
          Math.log((0.8 * (y3 - y1)) / this.landscapeHeightSize + 1.005) *
          65 *
          128 *
          ((this.width + this.length) / 500.0);

        // Set midpoint (colour of flat region) and clip to 0
        colourC += 137;
        colourC = Math.max(colourC, 0);

        // Clip to 255
        if (colourC > 255) {
          colourC = 255;
        }

        // Convert to rgb colours that look pretty nice
        var colourR = Math.min(colourC * 1.0 - 10, 255) * 0.95;
        var colourG = Math.min(colourC * 1.1, 255);
        var colourB = Math.min(colourC * 0.9 - 15, 255) * 0.9;

        // Mix in aerial imagery if it's provided
        if (aerial_rgb != null) {
          if (i < aerial_rgb.length) {
            if (j < aerial_rgb[i].length) {
              //colourR = (colourR + aerial_rgb[i][j][0])/2;
              //colourG = (colourG + aerial_rgb[i][j][1])/2;
              //colourB = (colourB + aerial_rgb[i][j][2])/2;
              colourR = (aerial_rgb[i][j][0] * (1.25 * colourC + 30)) / 255;
              colourG = (aerial_rgb[i][j][1] * (1.25 * colourC + 30)) / 255;
              colourB = (aerial_rgb[i][j][2] * (1.25 * colourC + 30)) / 255;

              // Tint landscape green
              colourR = colourR * 0.95;
              colourG = colourG * 1.15;
              colourB = colourB * 0.85;
            }
          }
        }
        arr2A.push([
          Math.max(0, Math.min(colourR, 255)),
          Math.max(0, Math.min(colourG, 255)),
          Math.max(0, Math.min(colourB, 255)),
        ]);
      }
      defaultColourArray.push(arr2A);
    }

    // We can't add colours after we've added the mesh to our scene, so we have to delete the old one and add a new one with colour from the landscapeArrayCaches
    this.scene.remove(this.landscapeMesh);
    this.landscapeMesh = this.generateLandscape(
      this.landscapeArrayCache,
      defaultColourArray
    );
    this.scene.add(this.landscapeMesh);

    // Add this image array to cache so we can revert to it later
    this.baseImageCache = this.clone3DArray(defaultColourArray);
    // console.log("Applied default texture");
  };

  /*
   * showOverlayImage
   * This function takes a 2D array of colours the same size as the mesh and overlays it onto the existing landscape mesh
   *
   */
  showBaseImage = function (newColourArray) {
    if (this.landscapeArrayCache == undefined) {
      console.error(
        'Overlay Image: Tried to overlay image onto mesh when no mesh was loaded'
      );
      return;
    }

    // We can't add colours after we've added the mesh to our scene, so we have to delete the old one and add a new one with colour from the landscapeArrayCaches
    this.scene.remove(this.landscapeMesh);
    this.landscapeMesh = this.generateLandscape(
      this.landscapeArrayCache,
      newColourArray
    );
    this.scene.add(this.landscapeMesh);

    // Add this image array to cache so we can revert to it later
    this.baseImageCache = this.clone3DArray(newColourArray);
  };

  /*
   * getOverlays()
   * runs after each refresh. deletes everything from the overlays cache and replaces it with stuff from the form
   * if we want to keep overlays in the state one day, that can be changed here 27/04/2021
   **/

  getOverlays = function () {
    this.addOverlay(
      0,
      this.trenchesEvenService.data,
      [
        this.trenchesEvenService.color?.rgb.r,
        this.trenchesEvenService.color?.rgb.g,
        this.trenchesEvenService.color?.rgb.b,
      ],
      this.trenchesEvenService.color?.rgb.a,
      this.trenchesEvenService.on
    );
    this.addOverlay(
      1,
      this.trenchesStepService.data,
      [
        this.trenchesStepService.color?.rgb.r,
        this.trenchesStepService.color?.rgb.g,
        this.trenchesStepService.color?.rgb.b,
      ],
      this.trenchesStepService.color?.rgb.a,
      this.trenchesStepService.on
    );
    this.addOverlay(
      2,
      this.gulliesService.data,
      [
        this.gulliesService.color?.rgb.r,
        this.gulliesService.color?.rgb.g,
        this.gulliesService.color?.rgb.b,
      ],
      this.gulliesService.color?.rgb.a,
      this.gulliesService.on
    );
    this.addOverlay(
      3,
      this.ridgesService.data,
      [
        this.ridgesService.color?.rgb.r,
        this.ridgesService.color?.rgb.g,
        this.ridgesService.color?.rgb.b,
      ],
      this.ridgesService.color?.rgb.a,
      this.ridgesService.on
    );
    this.addOverlay(
      4,
      this.cadastreService.data,
      [
        this.cadastreService.color?.rgb.r,
        this.cadastreService.color?.rgb.g,
        this.cadastreService.color?.rgb.b,
      ],
      this.cadastreService.color?.rgb.a,
      this.cadastreService.on
    );
    //this.addOverlay(5, this.layerPaddocks.data[0], [100, 236, 150], 0.70, this.layerPaddocks.on);
    //console.log("AAAAAA");
    //console.log(this.layerPaddocks.data.overlay);
  };

  /*
   * addOverlay()
   * Adds an overlay into the boolean overlays array, along with colour, opacity and visibility
   **/
  addOverlay = function (
    index,
    boolArrayy,
    overlayColourr,
    opacityy,
    visiblee
  ) {
    if (index > this.booleanMaps.length) {
      console.error(
        'addOverlay: tried to add an overlay at index ' +
          index +
          ' when max booleanmaps is ' +
          this.booleanMaps.length
      );
      return;
    }
    this.booleanMaps[index] = {
      boolArray: boolArrayy,
      overlayColour: overlayColourr,
      opacity: opacityy,
      visible: visiblee,
    };
  };

  /*
   * toggleOverlay()
   * This get's called when one of the switches is clicked. It then sets the corrosponding overlay's visibility to tru of false
   **/
  toggleOverlay = function (index, path, layerName, layerValue) {
    if (this.booleanMaps[index] == undefined) {
      console.error(
        'toggleOverlay: tried to modify visibility of overlay ' +
          index +
          ' when that index is undefined'
      );
      return;
    }
    var checkBox = document.getElementById('switch' + index);
    this.booleanMaps[index].visible = true; //checkBox.checked;
    // console.log ("toggleOverlay: Overlay " + index + " has been set to " + checkBox.checked + ".");
  };

  /*
   * toggleSatellite()
   * Turns the satellite imagery overlay on and off
   **/
  toggleSatellite = function () {
    if (this.aerialService.data != undefined) {
      if (this.aerialService.on) {
        this.showDefaultTexture(this.aerialService.data);
      } else {
        this.showDefaultTexture(null);
      }
    }
  };

  overlayBooleanMaps = function () {
    // console.log("Overlaying boolean maps");
    if (this.landscapeArrayCache == undefined) {
      console.warn(
        'Overlay Boolean: Tried to overlay image onto mesh when no mesh was loaded'
      );
      return;
    }

    // If there is no base image previously loaded then initialise it to sumthin idk
    if (this.baseImageCache == undefined) {
      console.warn(
        'Overlay Boolean: No base landscape colour is loaded yet. Defaulting to this awful magenta'
      );
      this.baseImageCache = new Array(this.width).fill(
        new Array(this.length).fill([255, 0, 255])
      );
    }

    // Copy our base colour array to a new one that we will apply to the mesh
    var colourArray = this.clone3DArray(this.baseImageCache);

    // ⢀⡴⠑⡄⠀⠀⠀⠀⠀⠀⠀⣀⣀⣤⣤⣤⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    // ⠸⡇⠀⠿⡀⠀⠀⠀⣀⡴⢿⣿⣿⣿⣿⣿⣿⣿⣷⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⠑⢄⣠⠾⠁⣀⣄⡈⠙⣿⣿⣿⣿⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⢀⡀⠁⠀⠀⠈⠙⠛⠂⠈⣿⣿⣿⣿⣿⠿⡿⢿⣆⠀⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⢀⡾⣁⣀⠀⠴⠂⠙⣗⡀⠀⢻⣿⣿⠭⢤⣴⣦⣤⣹⠀⠀⠀⢀⢴⣶⣆
    // ⠀⠀⢀⣾⣿⣿⣿⣷⣮⣽⣾⣿⣥⣴⣿⣿⡿⢂⠔⢚⡿⢿⣿⣦⣴⣾⠁⠸⣼⡿
    // ⠀⢀⡞⠁⠙⠻⠿⠟⠉⠀⠛⢹⣿⣿⣿⣿⣿⣌⢤⣼⣿⣾⣿⡟⠉⠀⠀⠀⠀⠀
    // ⠀⣾⣷⣶⠇⠀⠀⣤⣄⣀⡀⠈⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀
    // ⠀⠉⠈⠉⠀⠀⢦⡈⢻⣿⣿⣿⣶⣶⣶⣶⣤⣽⡹⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⠀⠀⠀⠉⠲⣽⡻⢿⣿⣿⣿⣿⣿⣿⣷⣜⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣷⣶⣮⣭⣽⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⠀⠀⣀⣀⣈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⠀⠀⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⠀⠀⠀⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀
    // ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠻⠿⠿⠿⠿⠛⠉
    // Mix up our colours (this is pretty epic ngl)
    for (var index = 0; index < this.booleanMaps.length; index++) {
      if (this.booleanMaps[index] != undefined) {
        if (this.booleanMaps[index].visible) {
          var boolArray = this.booleanMaps[index].boolArray;
          var overlayColour = this.booleanMaps[index].overlayColour;
          var opacity = this.booleanMaps[index].opacity;

          // console.log(boolArray);

          for (var i = 0; i < this.width - 1; i++) {
            for (var j = 0; j < this.length - 1; j++) {
              if (boolArray) {
                if (i > boolArray.width - 1) {
                  console.warn(
                    'Overlay Boolean X: Boolean map is smaller than landscape mesh: ' +
                      i
                  );
                  i = this.width + 1;
                  j = this.length + 1; // This is how i break loops. I know, shut up
                } else if (j > boolArray[0].length - 1) {
                  console.warn(
                    'Overlay Boolean Y: Boolean map is smaller than landsacpe mesh: ' +
                      j
                  );
                  i = this.width + 1;
                  j = this.length + 1;
                } else if (boolArray[i][j]) {
                  colourArray[i][j][0] = Math.min(
                    255,
                    overlayColour[0] * opacity +
                      colourArray[i][j][0] * (1.0 - opacity)
                  );
                  colourArray[i][j][1] = Math.min(
                    255,
                    overlayColour[1] * opacity +
                      colourArray[i][j][1] * (1.0 - opacity)
                  );
                  colourArray[i][j][2] = Math.min(
                    255,
                    overlayColour[2] * opacity +
                      colourArray[i][j][2] * (1.0 - opacity)
                  );
                } // O
              } // A
            } // M
          } // L
        } // Y
      } // Y
    } // A

    // Overlay paddocks (which are done a little bit differently to the other overlays)
    // Paddock data is not a boolean map, but instead a map of integers corrosponding to the paddock with that id's location.
    if (this.layerPaddocks.on && this.layerPaddocks.data != undefined) {
      // console.log(this.layerPaddocks);

      opacity = 0.5;
      for (var i = 0; i < this.width - 1; i++) {
        for (var j = 0; j < this.length - 1; j++) {
          if (i > this.layerPaddocks.data.overlay.width - 1) {
            console.warn(
              'Overlay Boolean X: Boolean map is smaller than landscape mesh: ' +
                i
            );
            i = this.width + 1;
            j = this.length + 1; // This is how i break loops. I know, shut up
          } else if (j > this.layerPaddocks.data.overlay[0].length - 1) {
            console.warn(
              'Overlay Boolean Y: Boolean map is smaller than landsacpe mesh: ' +
                j
            );
            i = this.width + 1;
            j = this.length + 1;
          } else if (this.layerPaddocks.data.overlay[i][j] != -1) {
            if (
              this.layerPaddocks.data.overlay[i][j] == this.highlightedPaddock
            ) {
              // this paddock is highlighted
              colourArray[i][j][0] = Math.min(
                255,
                180 * opacity * 1.5 +
                  colourArray[i][j][0] * (1.0 - opacity * 1.5)
              );
              colourArray[i][j][1] = Math.min(
                255,
                230 * opacity * 1.5 +
                  colourArray[i][j][1] * (1.0 - opacity * 1.5)
              );
              colourArray[i][j][2] = Math.min(
                255,
                255 * opacity * 1.5 +
                  colourArray[i][j][2] * (1.0 - opacity * 1.5)
              );
            } else {
              colourArray[i][j][0] = Math.min(
                255,
                this.hexToRGB(
                  this.layerPaddocks.colors[
                    this.layerPaddocks.data.overlay[i][j]
                  ]
                ).r *
                  opacity +
                  colourArray[i][j][0] * (1.0 - opacity)
              );
              colourArray[i][j][1] = Math.min(
                255,
                this.hexToRGB(
                  this.layerPaddocks.colors[
                    this.layerPaddocks.data.overlay[i][j]
                  ]
                ).g *
                  opacity +
                  colourArray[i][j][1] * (1.0 - opacity)
              );
              colourArray[i][j][2] = Math.min(
                255,
                this.hexToRGB(
                  this.layerPaddocks.colors[
                    this.layerPaddocks.data.overlay[i][j]
                  ]
                ).b *
                  opacity +
                  colourArray[i][j][2] * (1.0 - opacity)
              );
            }
          }
        }
      }
    }

    // We can't add colours after we've added the mesh to our scene, so we have to delete the old one and add a new one with colour from the landscapeArrayCaches
    this.scene.remove(this.landscapeMesh);
    this.landscapeMesh = this.generateLandscape(
      this.landscapeArrayCache,
      colourArray
    );
    this.scene.add(this.landscapeMesh);
  };

  // Detect mouseovers
  detectMouseover() {
    if (this.layerPaddocks.on && this.layerPaddocks.data != undefined) {
      // create a Ray with origin at the mouse position
      //   and direction into the scene (camera direction)
      var vector = new THREE.Vector3(this.mouse.x, this.mouse.y, 1);
      var ray = new THREE.Raycaster(
        this.camera.position,
        vector.sub(this.camera.position).normalize()
      );
      ray.setFromCamera(this.mouse, this.camera);

      // create an array containing all objects in the scene with which the ray intersects
      var intersects = ray.intersectObjects(this.scene.children);

      // if there is one (or more) intersections
      if (intersects.length > 0) {
        if (intersects[0].object.name == 'landscape') {
          // Get vertex on map that was hovered
          var faceID = intersects[0].faceIndex / 2;
          var selectionX = Math.floor(faceID / this.length);
          var selectionY = Math.floor(
            (faceID + Math.floor(faceID / this.length)) % this.length
          );

          // Check if we are mousing over a non-paddock area
          if (this.layerPaddocks.data.overlay[selectionX][selectionY] != -1) {
            // Check if the mouse has just moved around in the same paddock (so we don't redraw the mesh too many times)
            if (
              this.highlightedPaddock !=
              this.layerPaddocks.data.overlay[selectionX][selectionY]
            ) {
              this.highlightedPaddock =
                this.layerPaddocks.data.overlay[selectionX][selectionY];
              //console.log(selectionX + " " + selectionY + " " + this.highlightedPaddock);

              this.overlayBooleanMaps();

              this.onSetPaddockID(this.highlightedPaddock.toString());
              this.onSetPaddockArea(
                (
                  Math.round(
                    (this.layerPaddocks.data.areas[this.highlightedPaddock] /
                      100) *
                      100
                  ) / 100
                ).toString() + ' ha'
              );
              this.onSetPaddockPerimeter(
                (
                  Math.round(
                    this.layerPaddocks.data.perimeters[
                      this.highlightedPaddock
                    ] *
                      10 *
                      100
                  ) / 100
                ).toString() + ' m'
              );
            }
          } else if (this.highlightedPaddock != -1) {
            this.onSetPaddockNull();
            this.highlightedPaddock = -1;
            this.overlayBooleanMaps();
          }

          // We are not mosuing over the mesh, so set to -1 if not done already
        } else if (this.highlightedPaddock != -1) {
          this.onSetPaddockNull();
          this.highlightedPaddock = -1;
          this.overlayBooleanMaps();
        }

        //(this.scene.getObjectByName(intersects[0].object.name) as THREE.Mesh).material.//colour.setHex("0xffff00");
      } else {
        // there are no intersections
        if (this.highlightedPaddock != -1) {
          this.onSetPaddockNull();
          this.highlightedPaddock = -1;
          this.overlayBooleanMaps();
        }
      }
    }
  }

  animate() {
    window.requestAnimationFrame(() => this.animate());
    this.controls.update();
    this.renderer.render(this.scene, this.camera);
    this.detectMouseover();
  }

  clone3DArray = function (arr) {
    var arr2 = [];
    for (var a = 0; a < arr.length; a++) {
      var arr2A = [];
      for (var b = 0; b < arr[a].length; b++) {
        var arr2B = [];
        for (var c = 0; c < arr[a][b].length; c++) {
          arr2B.push(arr[a][b][c]);
        }
        arr2A.push(arr2B);
      }
      arr2.push(arr2A);
    }
    return arr2;
  };

  hexToRGB = function (hex) {
    /*var result = parseInt(hex, 16);
    var r = (result >> 16) & 255;
    var g = (result >> 8) & 255;
    var b = result & 255;
    return [r,g,b];*/
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result
      ? {
          r: parseInt(result[1], 16),
          g: parseInt(result[2], 16),
          b: parseInt(result[3], 16),
        }
      : null;
  };
}
