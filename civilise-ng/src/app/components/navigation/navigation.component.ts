import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  title = 'Civilise.AI';
  mainNavItems = [
    { title: "About", path: '/about' },
    { title: "Contact", path: '/contact' }
  ];
  sideNavItems = [
    ...this.mainNavItems
  ];

  isTablet$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 600px)')
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) { }

}
