import {
  Component,
  OnInit,
  NgZone,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';

/* 
  Bugfix:
    added @types/googlemaps: "3.39.12" to package.json
    https://github.com/DefinitelyTyped/DefinitelyTyped/issues/48574
*/

// TODO: Make rectangle select work in all directions
// TODO: bugfix - sometimes rectangle select doesn't register mouseup and keeps following select mode is disabled

interface rectangle {
  north: number;
  south: number;
  east: number;
  west: number;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  @Output() onSubmit = new EventEmitter<any>();

  zoom = 12;
  lat = -35.28878;
  lng = 149.0108;
  mapType = 'hybrid';
  selection: rectangle;
  fillColor: string;
  strokeColor: string;
  buttonColor: string;

  mapDraggable: boolean = true;

  map: google.maps.Map;
  mapMousedownListener;
  mapMouseMoveListener;
  zone: NgZone;

  rectStartLat;
  rectStartLng;

  constructor(zone: NgZone) {
    this.zone = zone;
  }

  ngOnInit(): void {
    this.resetSelection();
  }

  @HostListener('window:mouseup', ['$event'])
  onMouseUp(event) {
    this.strokeColor = 'black';
    this.fillColor = 'rebeccapurple';
    if (this.mapMouseMoveListener) this.mapMouseMoveListener.remove();
  }

  selectModeToggle(): void {
    this.mapDraggable = !this.mapDraggable;
    if (!this.mapDraggable) {
      this.buttonColor = 'primary';
      this.selectListeners(true);
    } else {
      this.buttonColor = 'none';
      this.selectListeners(false);
    }
  }

  selectListeners(on: boolean): void {
    if (on) {
      this.onMapMousedown();
    } else {
      this.mapMousedownListener.remove();
    }
  }

  resetSelection(): void {
    this.selection = {
      south: 0,
      north: 0,
      west: 0,
      east: 0,
    };
  }

  onMapMousedown(): void {
    // console.log("Event: Map Mouse Down");
    this.mapMousedownListener = this.map.addListener(
      'mousedown',
      (e: google.maps.MouseEvent) => {
        this.strokeColor = 'black';
        this.fillColor = 'transparent';
        this.zone.run(() => {
          this.rectStartLat = e.latLng.lat();
          this.rectStartLng = e.latLng.lng();
          this.selection.north = e.latLng.lat();
          this.selection.south = e.latLng.lat();
          this.selection.west = e.latLng.lng();
          this.selection.east = e.latLng.lng();
          this.onMapMousemove();
        });
      }
    );
  }

  onMapMousemove(): void {
    this.mapMouseMoveListener = this.map.addListener(
      'mousemove',
      (e: google.maps.MouseEvent) => {
        this.zone.run(() => {
          var lonDiff: number = Math.abs(
            Math.max(this.rectStartLng, e.latLng.lng()) -
              Math.min(this.rectStartLng, e.latLng.lng())
          );
          var latDiff: number = Math.abs(
            Math.min(this.rectStartLat, e.latLng.lat()) -
              Math.max(this.rectStartLat, e.latLng.lat())
          );
          if (lonDiff * latDiff < 0.004) {
            this.strokeColor = 'black';
            this.fillColor = 'rebeccapurple';
            if (e.latLng.lng() < this.rectStartLng) {
              // box has moved left
              this.selection.east = this.rectStartLng;
              this.selection.west = e.latLng.lng();
            } else {
              // box has moved right
              this.selection.west = this.rectStartLng;
              this.selection.east = e.latLng.lng();
            }
            if (e.latLng.lat() > this.rectStartLat) {
              // box has moved up
              this.selection.south = this.rectStartLat;
              this.selection.north = e.latLng.lat();
            } else {
              // box has moved down
              this.selection.north = this.rectStartLat;
              this.selection.south = e.latLng.lat();
            }
          } else {
            this.strokeColor = 'firebrick';
            this.fillColor = 'deeppink';
          }
        });
      }
    );
  }

  /* 
  Issue with using (mapClick)="mapClicked($event)" on <agm-map>
  $event returns 'c' not the desired object with lat and lng
  need map ready with click listener as work around, see link:
  https://github.com/SebastianM/angular-google-maps/issues/1845
  */
  public onMapReady(map: google.maps.Map): void {
    // https://stackoverflow.com/questions/46873408/angular-4-google-map-full-control-and-zoom-control-positions-and-styling

    // console.log("Map Ready");
    this.map = map;
    this.map.setOptions({
      fullscreenControl: false,
      mapTypeControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
      },
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM,
        style: google.maps.ZoomControlStyle.DEFAULT,
      },
    });
  }

  public ngOnDestroy(): void {
    if (this.mapMousedownListener) this.mapMousedownListener.remove();
    if (this.mapMouseMoveListener) this.mapMouseMoveListener.remove();
  }

  onStart(): void {
    let coords = `${this.selection.north},${this.selection.west};${this.selection.south},${this.selection.east}`;
    if (coords) {
      // console.log(coords)
      this.onSubmit.emit(coords);
    }
  }

  trimLatLng(str: string): string {
    let rtn = '';
    let tmp = str.split('.');
    rtn += tmp[0] + '.' + tmp[1].substr(0, 6);
    // rtn += tmp[0] + "." + tmp[1].substr(0, 4) + "..." + tmp[1].substr(tmp[1].length - 2, tmp[1].length);
    return rtn;
  }
}
