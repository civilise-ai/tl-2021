import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaddocksComponent } from './paddocks.component';

describe('PaddocksComponent', () => {
  let component: PaddocksComponent;
  let fixture: ComponentFixture<PaddocksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaddocksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaddocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
