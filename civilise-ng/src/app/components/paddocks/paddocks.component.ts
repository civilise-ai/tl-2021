import { Component, Input, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Layers } from 'three';

import { LayerService } from '../../services/layers.service';


@Component({
  selector: 'app-paddocks',
  templateUrl: './paddocks.component.html',
  styleUrls: ['./paddocks.component.scss']
})
export class PaddocksComponent implements AfterViewInit {
  @Input() paddocks: any;
  transformedPaddocks: any = [];

  displayedColumns: string[] = ['_id', 'area', 'perimeter'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  metricArea: boolean = true;

  constructor( private layerService: LayerService ) { 

  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.transformedPaddocks);
  }

  ngOnChanges() {
    this.transformedPaddocks = []; // reset paddocks to avoid accum concat
    if (this.paddocks && this.paddocks.data?.areas) {
      for (let i = 0; i < this.paddocks.data.areas.length; i++) {
        this.transformedPaddocks.push({
          _id: i,
          area: this.paddocks.data.areas[i],
          perimeter: this.paddocks.data.perimeters[i],
          color: this.paddocks.colors[i]
        });
      }
    }
    this.dataSource = new MatTableDataSource(this.transformedPaddocks);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

  toggleMetricArea(event) {
    this.metricArea = !this.metricArea;
    event.stopPropagation();
  }

  getAreaAcres(value) {
    let rtn = value;
    // * 100 then / 100 to round to 2dp
    rtn = Math.round((value / 40.4686) * 100) / 100;
    return rtn;
  }

  getAreaHectares(value) {
    let rtn = value;
    // * 100 then / 100 to round to 2dp
    rtn = Math.round((value / 100) * 100) / 100;
    return rtn;
  }

  getPerimeter(value) {
    let rtn = value;
    // * 100 then / 100 to round to 2dp
    rtn = Math.round((value * 10) * 100) / 100;
    return rtn;
  }

  exportPaddockData() {
    console.log("exporting data....");
    //console.log(this.paddocks)
    //console.log(this.cleanFenceLines(this.paddocks.data.overlay))
    //console.log(this.padWithFence(this.paddocks.data.overlay))
    //console.log(this.isolateFenceLines(this.paddocks.data.overlay, 0))
    let workingOverlay = this.cleanFenceLines(this.paddocks.data.overlay)
    workingOverlay = this.padWithFence(workingOverlay);
    let isolatedFenceLines = new Array(this.paddocks.data.areas.length)
    for (let i = 0; i < isolatedFenceLines.length; i++) {
      isolatedFenceLines[i] = this.isolateFenceLines(workingOverlay, i);
    }
    //console.log(isolatedFenceLines);
    //console.log(this.adjacentTilesArray(workingOverlay, 3, 3))
    //console.log(this.traceBoundary(isolatedFenceLines[0]))
    let features = []
    for (let i = 0; i < isolatedFenceLines.length; i++) {

      let geometry = {
        type: "Polygon", coordinates: [this.traceBoundary(isolatedFenceLines[i])]
      }
      let properties = {area: this.paddocks.data.areas[i], perimeter : this.paddocks.data.perimeters[i]}
      let feature = { type: "Feature", properties: properties, "geometry": geometry }
      features.push(feature)
    }
    let data = { type: "FeatureCollection", features: features };
    let exportData = JSON.stringify(data);
    var blob = new Blob([exportData], { type: 'application/geo+json' });
    var url = window.URL.createObjectURL(blob);
    var a = document.createElement("a");
    a.href = url;
    a.download = "test.geojson";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  exportLayerData(){
    console.log("Clicked request layer output");
    // this.layerService.httpRequestExport().subscribe( (result) => {document.location.href = this.layerService.api + "/" + result?.exportFile});
    this.layerService.httpRequestExport().subscribe( (result) => {
      // console.log(result);

      // var newWindow = window.open();
      // newWindow.document.write(result);

      if ('trenches_even' in result){
        let exportData = JSON.stringify(result.trenches_even).replace(/\\/g, '').slice(1).slice(0, -1);
        var blob = new Blob([exportData], { type: 'application/geo+json' });
        var url = window.URL.createObjectURL(blob);
        var a = document.createElement("a");
        a.href = url;
        a.download = "trenches_even.geojson";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }

      if ('trenches_step' in result){
        let exportData2 = JSON.stringify(result.trenches_step).replace(/\\/g, '').slice(1).slice(0, -1);
        var blob2 = new Blob([exportData2], { type: 'application/geo+json' });
        var url2 = window.URL.createObjectURL(blob2);
        var b = document.createElement("a");
        b.href = url2;
        b.download = "trenches_step.geojson";
        document.body.appendChild(b);
        b.click();
        document.body.removeChild(b);
      }

      if ('gullies_pysheds' in result){
        let exportData3 = JSON.stringify(result.gullies_pysheds).replace(/\\/g, '').slice(1).slice(0, -1);
        var blob3 = new Blob([exportData3], { type: 'application/geo+json' });
        var url3 = window.URL.createObjectURL(blob3);
        var c = document.createElement("a");
        c.href = url3;
        c.download = "gullies.geojson";
        document.body.appendChild(c);
        c.click();
        document.body.removeChild(c);
      }

      if ('ridges_pysheds' in result){
        let exportData4 = JSON.stringify(result.ridges_pysheds).replace(/\\/g, '').slice(1).slice(0, -1);
        var blob4 = new Blob([exportData4], { type: 'application/geo+json' });
        var url4 = window.URL.createObjectURL(blob4);
        var d = document.createElement("a");
        d.href = url4;
        d.download = "ridges.geojson";
        document.body.appendChild(d);
        d.click();
        document.body.removeChild(d);
      }

      if ('paddocks' in result){
        let exportData5 = JSON.stringify(result.paddocks).replace(/\\/g, '').slice(1).slice(0, -1);
        var blob5 = new Blob([exportData5], { type: 'application/geo+json' });
        var url5 = window.URL.createObjectURL(blob5);
        var e = document.createElement("a");
        e.href = url5;
        e.download = "paddocks.geojson";
        document.body.appendChild(e);
        e.click();
        document.body.removeChild(e);
      }

      if ('cadastre' in result){
        let exportData6 = JSON.stringify(result.cadastre).replace(/\\/g, '').slice(1).slice(0, -1);
        var blob6 = new Blob([exportData6], { type: 'application/geo+json' });
        var url6 = window.URL.createObjectURL(blob6);
        var f = document.createElement("a");
        f.href = url6;
        f.download = "cadastre.geojson";
        document.body.appendChild(f);
        f.click();
        document.body.removeChild(f);
      }

    });
  }


  cleanFenceLines(overlay) {
    let newOverlay = new Array(overlay.length);
    for (let i = 0; i < overlay.length; i++) {
      let column = new Array(overlay[0].length)
      for (let j = 0; j < overlay[0].length; j++) {
        if (overlay[i][j] == -1) {
          let adjacent = this.adjacentTilesSet(overlay, i, j)
          if (adjacent.size == 1) {
            column[j] = adjacent.entries().next().value[0]
          } else {
            column[j] = overlay[i][j];
          }
        } else {
          column[j] = overlay[i][j];
        }
      }
      newOverlay[i] = column;
    }
    return newOverlay;
  }

  adjacentTilesSet(overlay, x, y) {
    let adjacentSet = new Set();
    if (x > 0) {
      if (overlay[x-1][y] > -1) {
        adjacentSet.add(overlay[x - 1][y])
      }
    }
    if (x < overlay.length - 1) {
      if (overlay[x + 1][y] > -1) {
        adjacentSet.add(overlay[x + 1][y])
      }
    }
    if (y > 0) {
      if (overlay[x][y-1] > -1) {
        adjacentSet.add(overlay[x][y-1])
      }
    }
    if (y < overlay[0].length - 1) {
      if (overlay[x][y+1] > -1) {
        adjacentSet.add(overlay[x][y+1])
      }
    }
    return adjacentSet;
  }

  //need to refactor x and y (they are actually row and column)
  adjacentTilesArray(overlay, x, y) {
    let adjacentArray = new Array(8);
    switch (x) {
      case 0:
        adjacentArray[0] = -10
        adjacentArray[1] = -10
        adjacentArray[2] = overlay[x][y + 1]
        adjacentArray[3] = overlay[x + 1][y + 1]
        adjacentArray[4] = overlay[x + 1][y]
        adjacentArray[5] = overlay[x + 1][y - 1]
        adjacentArray[6] = overlay[x][y - 1]
        adjacentArray[7] = -10
        break;
      case overlay.length - 1:
        adjacentArray[0] = overlay[x - 1][y]
        adjacentArray[1] = overlay[x - 1][y + 1]
        adjacentArray[2] = overlay[x][y + 1]
        adjacentArray[3] = -10
        adjacentArray[4] = -10
        adjacentArray[5] = -10
        adjacentArray[6] = overlay[x][y - 1]
        adjacentArray[7] = overlay[x - 1][y - 1]
        break;
      default:
        adjacentArray[0] = overlay[x - 1][y]
        adjacentArray[1] = overlay[x - 1][y + 1]
        adjacentArray[2] = overlay[x][y + 1]
        adjacentArray[3] = overlay[x + 1][y + 1]
        adjacentArray[4] = overlay[x + 1][y]
        adjacentArray[5] = overlay[x + 1][y - 1]
        adjacentArray[6] = overlay[x][y - 1]
        adjacentArray[7] = overlay[x - 1][y - 1]
    }
    for (let i = 0; i < adjacentArray.length; i++) {
      if (adjacentArray[i] == undefined) {
        adjacentArray[i] = -10
      }
    }
    return adjacentArray
  }

  isolateFenceLines(overlay, paddockNumber) {
    let isolatedOverlay = new Array(overlay.length);
    for (let i = 0; i < overlay.length; i++) {
      let row = new Array(overlay[0].length)
      for (let j = 0; j < row.length; j++) {
        if (overlay[i][j] == paddockNumber) {
          row[j] = paddockNumber;
        } else {
          let adjacent = this.adjacentTilesSet(overlay, i, j)
          if (adjacent.has(paddockNumber)) {
            row[j] = -1
          } else {
            row[j] = -10;
          }
        }
      }
      isolatedOverlay[i] = row;
    }
    return isolatedOverlay;
  }

  padWithFence(overlay) {
    let padded = new Array(overlay.length + 2);
    let dummyRow = new Array(overlay[0].length + 2)
    for (let i = 0; i < dummyRow.length; i++) {
      dummyRow[i] = -1;
    }
    padded[0] = dummyRow;
    padded[padded.length - 1] = dummyRow;

    for (let i = 1; i < padded.length-1; i++) {
      let row = new Array(overlay[0].length + 2)
      row[0] = -1;
      row[row.length - 1] = -1;
      for (let j = 1; j < row.length-1; j++) {
        row[j] = overlay[i-1][j-1]
      }
      padded[i] = row;
    }
    return padded;
  }

  traceBoundary(overlay) {
    let path = new Array();
    let found = false;
    let firstpoint = new Array(2);
    for (let i = 0; i < overlay.length; i++) {
      for (let j = 0; j < overlay[0].length; j++) {
        if (found == false && overlay[i][j] == -1) {
          firstpoint[0] = i;
          firstpoint[1] = j;
          found = true;
        }
      }
    }
    let complete = false;
    let currentpoint = firstpoint;
    let trajectory = 2;
    path.push([currentpoint[0],currentpoint[1]]);
    while (complete == false) {
      let nextPoint = this.findNextOnPath(overlay,currentpoint, trajectory, path)
      path.push([nextPoint[0][0], nextPoint[0][1]]);
      currentpoint[0] = nextPoint[0][0];
      currentpoint[1] = nextPoint[0][1];
      trajectory = Number(nextPoint[1])
      if ((nextPoint[0][0] == path[0][0] && nextPoint[0][1] == path[0][1]) || path.length > 10000) {
        complete = true;
      }
    }
    return path.reverse();
  }

  findNextOnPath(overlay, point, trajectory, prevPath) {
    let newtrajectory = trajectory - 2;
    if (newtrajectory < 0) {
      newtrajectory = newtrajectory + 8;
    }
    let foundNext = false;
    let adjacentArray = this.adjacentTilesArray(overlay,point[0],point[1])

    while (foundNext == false) {
      if (adjacentArray[newtrajectory] == -1 ) {
        foundNext = true;
      }
      if (foundNext == false) {
        newtrajectory = newtrajectory + 1;
      }
      if (newtrajectory > 7) {
        newtrajectory = newtrajectory - 8;
      }
    }
    let newPoint = this.pointFromTrajectory(newtrajectory, point)
    return [newPoint, newtrajectory]
  }

  pointFromTrajectory(trajectory, point) {
    let newPoint = new Array(2);
    switch (trajectory) {
      case 0:
        newPoint[0] = point[0] - 1;
        newPoint[1] = point[1];
        break;
      case 1:
        newPoint[0] = point[0] - 1;
        newPoint[1] = point[1] + 1;
        break;
      case 2:
        newPoint[0] = point[0];
        newPoint[1] = point[1] + 1;
        break;
      case 3:
        newPoint[0] = point[0] + 1;
        newPoint[1] = point[1] + 1;
        break;
      case 4:
        newPoint[0] = point[0] + 1;
        newPoint[1] = point[1];
        break;
      case 5:
        newPoint[0] = point[0] + 1;
        newPoint[1] = point[1] - 1;
        break;
      case 6:
        newPoint[0] = point[0];
        newPoint[1] = point[1] - 1;
        break;
      case 7:
        newPoint[0] = point[0] - 1;
        newPoint[1] = point[1] - 1;
        break;
    }
    return newPoint
  }
  
}
