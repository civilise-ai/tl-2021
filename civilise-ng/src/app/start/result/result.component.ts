import {
  Component,
  Input,
  Output,
  EventEmitter,
  Renderer2,
  ElementRef,
  ViewChild,
} from '@angular/core';
import {
  LayerService,
  Layer,
  PaddocksLayer,
} from '../../services/layers.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})
export class ResultComponent {
  @Input() paramTerrainExaggerationOn: boolean;
  @Input() paramTerrainExaggerationVal: string;
  @Output() changedTerrainExaggerationOn = new EventEmitter<any>();
  @Output() changedTerrainExaggerationVal = new EventEmitter<any>();
  loadingTerrainExaggeration: boolean;

  onChangedTerrainExaggerationOn() {
    this.paramTerrainExaggerationVal = '1';
    this.changedTerrainExaggerationOn.emit(this.paramTerrainExaggerationOn);
  }

  onChangedTerrainExaggerationVal(val) {
    this.loadingTerrainExaggeration = true;
    this.changedTerrainExaggerationVal.emit(val);
    this.paramTerrainExaggerationVal = val;
    this.loadingTerrainExaggeration = false;
  }

  paddockArea: string;
  paddockPerimeter: string;
  paddockID: string;

  setPaddockID(value): void {
    // console.log(value);
    this.paddockID = value;
  }
  setPaddockArea(value): void {
    // console.log(value);
    this.paddockArea = value;
  }
  setPaddockPerimeter(value): void {
    // console.log(value);
    this.paddockPerimeter = value;
  }
  setPaddockNull(): void {
    this.paddockID = null;
    this.paddockArea = null;
    this.paddockPerimeter = null;
  }

  openedLeftDrawer: boolean = false;
  openedRightDrawer: boolean = true;

  dem: Layer = {} as Layer;
  aerial: Layer = {} as Layer;
  cadastre: Layer = {} as Layer;
  trenchesEven: Layer = {} as Layer;
  trenchesStep: Layer = {} as Layer;
  gullies: Layer = {} as Layer;
  ridges: Layer = {} as Layer;
  paddocks: PaddocksLayer = {} as PaddocksLayer;

  colors = [
    '#64ec96',
    '#421e0a',
    '#024f69',
    '#2E34A6',
    '#fceb03',
    '#663399',
    '#CC2FBF',
    '#459923',
  ];

  colorSelectCadastreOn: boolean = false;
  @ViewChild('colorSelectCadastreElement')
  colorSelectCadastreElement: ElementRef;

  colorSelectTrenchesEvenOn: boolean = false;
  @ViewChild('colorSelectTrenchesEvenElement')
  colorSelectTrenchesEvenElement: ElementRef;

  colorSelectTrenchesStepOn: boolean = false;
  @ViewChild('colorSelectTrenchesStepElement')
  colorSelectTrenchesStepElement: ElementRef;

  colorSelectGulliesOn: boolean = false;
  @ViewChild('colorSelectGulliesElement')
  colorSelectGulliesElement: ElementRef;

  colorSelectRidgesOn: boolean = false;
  @ViewChild('colorSelectRidgesElement')
  colorSelectRidgesElement: ElementRef;

  constructor(private layerService: LayerService, private renderer: Renderer2) {
    this.layerService.getDem.subscribe((dem) => (this.dem = dem));
    this.layerService.getAerial.subscribe((aerial) => (this.aerial = aerial));
    this.layerService.getCadastre.subscribe(
      (cadastre) => (this.cadastre = cadastre)
    );
    this.layerService.getTrenchesEven.subscribe(
      (trenches) => (this.trenchesEven = trenches)
    );
    this.layerService.getTrenchesStep.subscribe(
      (trenches) => (this.trenchesStep = trenches)
    );
    this.layerService.getGullies.subscribe(
      (gullies) => (this.gullies = gullies)
    );
    this.layerService.getRidges.subscribe((ridges) => (this.ridges = ridges));
    this.layerService.getPaddocks.subscribe((paddocks) => {
      this.paddocks = JSON.parse(JSON.stringify(paddocks)); // call by ref doesn't seem to regen the three component on changes, so making copy and using the copy
    });

    /**
     * This events get called by all clicks on the page
     */
    this.renderer.listen('window', 'click', (e: Event) => {
      if (!this.colorSelectCadastreElement?.nativeElement?.contains(e.target)) {
        this.colorSelectCadastreOn = false;
      }

      if (
        !this.colorSelectTrenchesEvenElement?.nativeElement?.contains(e.target)
      ) {
        this.colorSelectTrenchesEvenOn = false;
      }

      if (
        !this.colorSelectTrenchesStepElement?.nativeElement?.contains(e.target)
      ) {
        this.colorSelectTrenchesStepOn = false;
      }

      if (!this.colorSelectGulliesElement?.nativeElement?.contains(e.target)) {
        this.colorSelectGulliesOn = false;
      }

      if (!this.colorSelectRidgesElement?.nativeElement?.contains(e.target)) {
        this.colorSelectRidgesOn = false;
      }
    });
  }

  onChangeAerialOn(): void {
    this.layerService.setAerial({ on: this.aerial.on });
  }

  onChangeCadastreOn() {
    this.layerService.setCadastre({ on: this.cadastre.on });
  }

  onChangeTrenchesEvenOn() {
    this.layerService.setTrenchesEven({ on: this.trenchesEven.on });
  }

  onChangeTrenchesEvenVal(value) {
    this.layerService.setTrenchesEven({ value: value, loading: true });
    this.layerService
      .httpDefault({ trenchesEven: 'placeholder' }, {}, {})
      .subscribe((result) => {
        this.layerService.setTrenchesEven({
          loading: false,
          data: result.trenchesEven.array,
        });
      });
  }

  onChangeTrenchesStepOn() {
    this.layerService.setTrenchesStep({ on: this.trenchesStep.on });
  }

  onChangeTrenchesStepVal(value) {
    this.layerService.setTrenchesStep({ value: value, loading: true });
    // Using a placeholder because we can't access the actual parameters from here (need to be in layers.service.ts)
    this.layerService
      .httpDefault({ trenchesStep: 'placeholder' }, {}, {})
      .subscribe((result) => {
        this.layerService.setTrenchesStep({
          loading: false,
          data: result.trenchesStep.array,
        });
      });
  }

  onChangeGulliesOn() {
    this.layerService.setGullies({ on: this.gullies.on });
  }

  onChangeGulliesVal(value) {
    this.layerService.setGullies({ value: value, loading: true });

    this.layerService
      .httpDefault({ gullies: 'placeholder' }, {}, {})
      .subscribe((result) => {
        this.layerService.setGullies({
          loading: false,
          data: result.gullies.array,
        });
      });
  }

  onChangeRidgesOn() {
    this.layerService.setRidges({ on: this.ridges.on });
  }

  onChangeRidgesVal(value) {
    this.layerService.setRidges({ value: value, loading: true });
    this.layerService
      .httpDefault({ ridges: 'placeholder' }, {}, {})
      .subscribe((result) => {
        this.layerService.setRidges({
          loading: false,
          data: result.ridges.array,
        });
      });
  }

  onChangePaddocksOn() {
    this.layerService.setPaddocks({ on: this.paddocks.on });
  }

  onChangeColorCadastre(event) {
    this.layerService.setCadastre({ color: event.color });
  }

  onChangeColorTrenchesEven(event) {
    this.layerService.setTrenchesEven({ color: event.color });
  }

  onChangeColorTrenchesStep(event) {
    this.layerService.setTrenchesStep({ color: event.color });
  }

  onChangeColorGullies(event) {
    this.layerService.setGullies({ color: event.color });
  }

  onChangeColorRidges(event) {
    this.layerService.setRidges({ color: event.color });
  }

  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }
    return value;
  }
}
