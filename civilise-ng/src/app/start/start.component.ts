import { Component, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';

import { LayerService, Layer } from '../services/layers.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent{
  @ViewChild('tabs') tabGroup: MatTabGroup;

  dem: Layer;

  constructor(private layerService: LayerService) {
    this.layerService.getDem.subscribe((dem: Layer) => {
      this.dem = dem;
      if (!this.dem.loading) this.tabGroup.selectedIndex = 1;
    });
  }

  ngAfterContentInit(): void {
    // load query params and update the apps starting state accordingly
    this.layerService.queryParamsGet();
  }
}
