import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

// TODO: reset layers on new search

// TODO: make httpSingleLayer api call into individual single layer calls / requires adding endpoints to flask and to add methods here that are connected up on result component

// TODO: add terrain exagetation to query string management

import { MessageService } from './message.service';

const debug = true;

export interface Layer {
  on?: boolean;
  value?: string;
  data?: any;
  loading?: boolean;
  color?: any;
}

export interface PaddocksLayer {
  on?: boolean;
  data?: any;
  colors?: any;
}

@Injectable({ providedIn: 'root' })
export class LayerService {
  public api = window.location.origin + '/api';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  /*
   *  DEM:   props, get, set
   **/
  private dem: Layer = {
    value: undefined, // coords
    data: undefined,
    loading: undefined, // start off as null to indicate not loading nor loaded
    // can we record the shape and parsed_coords here too?
  };
  setDem = (dem: Layer) => {
    if (debug) console.log('setDem()');
    // update the layer state
    if (dem.value) this.dem.value = dem.value;
    if (dem.data) this.dem.data = dem.data;
    if (dem.loading) this.dem.loading = dem.loading;
    // update the url query string
    this.queryParamsSet('coords', this.dem.value);
    // trigger loading/loaded state transitions used for searching to found result
    this.getDem.next(this.dem);
    // call the api - this is a brand new search. Use all (but only) the parameters specifies in the url
    this.httpDefault(
      {
        coords: this.dem.value,
      },
      {},
      {}
    ).subscribe(async (result) => {
      // DEM
      this.dem.loading = false;
      this.dem.data = result.DEM;
      this.getDem.next(this.dem);
      this.prevParams.coords = this.dem.value;

      // DEM has been loaded -> async load the default layer settings
      this.setAerial(this.aerial);
      this.setCadastre(this.cadastre);
      this.setTrenchesEven(this.trenchesEven);
      this.setTrenchesStep(this.trenchesStep);
      this.setGullies(this.gullies);
      this.setRidges(this.ridges);
      this.setPaddocks(this.paddocks);

      // Testing multiple layers can be queries at the same time
      // this.setAerial({data: result.aerial});
      // this.setCadastre({data: result.cadastre});
      // this.setTrenchesEven({ loading: false, data: result.trenchesEven.array });
      // this.setTrenchesStep({ loading: false, data: result.trenchesStep.array });
      // this.setGullies({ loading: false, data: result.gullies.array });
      // this.setRidges({ loading: false, data: result.ridges.array });
      // this.setPaddocks({ data: result.paddocks });
    });
    // Might need to reset all the other layers here too?
  };
  getDem = new Subject<any>();

  /*
   *  Aerial:   props, get, set
   **/
  private aerial: Layer = {
    on: true,
    data: undefined,
  };
  setAerial = (aerial: Layer) => {
    if (debug) {
      console.log('setAerial()');
      console.log(aerial);
    }
    // update the layer state
    if (aerial.on) this.aerial.on = aerial.on;
    if (aerial.data) this.aerial.data = aerial.data;
    if (this.aerial.on && !this.aerial.data) {
      this.httpDefault({ aerial: 'placeholder' }, {}, {}).subscribe(
        (result) => {
          this.setAerial({ data: result.aerial });
        }
      );
    }
    // Display the layer
    this.getAerial.next(this.aerial);
    // update query params in url
    this.queryParamsSet('aerial-on', this.aerial.on);
  };
  getAerial = new Subject<any>();

  /*
   *  Cadastre:   props, get, set
   **/
  private cadastre: Layer = {
    on: false,
    data: undefined,
    color: {
      hex: '#64ec96',
      rgb: { r: 100, g: 236, b: 150, a: 0.75 },
    },
  };
  setCadastre = (cadastre: Layer) => {
    if (debug) console.log('setCadastre()');
    // update the layer state
    if (cadastre.on) this.cadastre.on = cadastre.on;
    if (cadastre.data) this.cadastre.data = cadastre.data;
    if (cadastre.color) this.cadastre.color = cadastre.color;
    if (this.cadastre.on && !this.cadastre.data) {
      this.httpDefault({ cadastre: 'placeholder' }, {}, {}).subscribe(
        (result) => {
          this.setCadastre({ data: result.cadastre });
        }
      );
    }
    // Display the layer
    this.getCadastre.next(this.cadastre);
    // update query params in url
    this.queryParamsSet('cadastre-on', this.cadastre.on);
  };
  getCadastre = new Subject<any>();

  /*
   *  Trenches Even:   props, get, set
   **/
  private trenchesEven: Layer = {
    on: true,
    value: '4',
    color: {
      hex: '#421e0a',
      rgb: { r: 66, g: 30, b: 10, a: 0.75 },
    },
  };
  setTrenchesEven = async (trenches: Layer) => {
    if (debug) {
      console.log('setTrenchesEven()');
      console.log(trenches);
    }
    let regenPaddocks = false;
    // update the layer state
    if (trenches.on) this.trenchesEven.on = trenches.on;
    if (trenches.value) {
      this.trenchesEven.value = trenches.value;
      regenPaddocks = true;
    }
    if (trenches.data) this.trenchesEven.data = trenches.data;
    if (trenches.color) this.trenchesEven.color = trenches.color;
    if (trenches.loading !== undefined)
      this.trenchesEven.loading = trenches.loading;
    // http call if needed by update
    if (this.trenchesEven.on && !this.trenchesEven.data) {
      this.httpDefault({ trenchesEven: 'placeholder' }, {}, {}).subscribe(
        (result) => {
          this.setTrenchesEven({
            loading: false,
            data: result.trenchesEven.array,
          });
        }
      );
    }
    // recalculate paddocks
    if (regenPaddocks) this.setPaddocks({ data: null });
    // Update the slider
    this.getTrenchesEven.next(this.trenchesEven);
    // update query params in url
    await this.queryParamsSet('trenches-even-on', this.trenchesEven.on);
    await this.queryParamsSet('trenches-even-val', this.trenchesEven.value);
  };
  getTrenchesEven = new Subject<any>();

  /*
   *  Trenches Step:   props, get, set
   **/
  private trenchesStep: Layer = {
    on: false,
    value: '4',
    color: {
      hex: '#024f69',
      rgb: { r: 2, g: 79, b: 105, a: 0.75 },
    },
  };
  setTrenchesStep = async (trenches: Layer) => {
    if (debug) console.log('setTrenchesStep()');
    let regenPaddocks = false;
    // update the layer state
    if (trenches.on) this.trenchesStep.on = trenches.on;
    if (trenches.value) {
      this.trenchesStep.value = trenches.value;
      regenPaddocks = true;
    }
    if (trenches.data) this.trenchesStep.data = trenches.data;
    if (trenches.color) this.trenchesStep.color = trenches.color;
    if (trenches.loading !== undefined)
      this.trenchesStep.loading = trenches.loading;
    if (this.trenchesStep.on && !this.trenchesStep.data) {
      this.httpDefault({ trenchesStep: 'placeholder' }, {}, {}).subscribe(
        (result) => {
          this.setTrenchesStep({
            loading: false,
            data: result.trenchesStep.array,
          });
        }
      );
    }
    // Update the slider
    if (regenPaddocks) this.setPaddocks({ data: null });
    // Update the slider
    this.getTrenchesStep.next(this.trenchesStep);
    // update the url query string
    await this.queryParamsSet('trenches-step-on', this.trenchesStep.on);
    await this.queryParamsSet('trenches-step-val', this.trenchesStep.value);
  };
  getTrenchesStep = new Subject<any>();

  /*
   *  Gullies:   props, get, set
   **/
  private gullies: Layer = {
    on: true,
    value: '99',
    color: {
      hex: '#2E34A6',
      rgb: { r: 46, g: 52, b: 166, a: 0.75 },
    },
  };
  setGullies = async (gullies: Layer) => {
    if (debug) console.log('setGullies()');
    let regenPaddocks = false;
    // update the layer state
    if (gullies.on) this.gullies.on = gullies.on;
    if (gullies.value) {
      this.gullies.value = gullies.value;
      regenPaddocks = true;
    }
    if (gullies.data) this.gullies.data = gullies.data;
    if (gullies.color) this.gullies.color = gullies.color;
    if (gullies.loading !== undefined) this.gullies.loading = gullies.loading;
    // call API if needed
    if (this.gullies.on && !this.gullies.data) {
      this.httpDefault({ gullies: 'placeholder' }, {}, {}).subscribe(
        (result) => {
          this.setGullies({ loading: false, data: result.gullies.array });
          // this.layerService.setPaddocks({ data: result.paddocks });
        }
      );
    }
    // recalculate paddocks
    if (regenPaddocks) this.setPaddocks({ data: null });
    // update the slider
    this.getGullies.next(this.gullies);
    // update the url query string
    await this.queryParamsSet('gullies-on', this.gullies.on);
    await this.queryParamsSet('gullies-val', this.gullies.value);
  };
  getGullies = new Subject<any>();

  /*
   *  Ridges:   props, get, set
   **/
  private ridges: Layer = {
    on: true,
    value: '99',
    color: {
      hex: '#fceb03',
      rgb: { r: 252, g: 236, b: 3, a: 0.75 },
    },
  };
  setRidges = async (ridges: Layer) => {
    if (debug) console.log('setRidges()');
    let regenPaddocks = false;
    // update the layer state
    if (ridges.on) this.ridges.on = ridges.on;
    if (ridges.value) {
      this.ridges.value = ridges.value;
      regenPaddocks = true;
    }
    if (ridges.data) this.ridges.data = ridges.data;
    if (ridges.color) this.ridges.color = ridges.color;
    if (ridges.loading !== undefined) this.ridges.loading = ridges.loading;
    // call api if needed
    if (this.ridges.on && !this.ridges.data) {
      this.httpDefault({ ridges: 'placeholder' }, {}, {}).subscribe(
        (result) => {
          this.setRidges({ loading: false, data: result.ridges.array });
        }
      );
    }
    // recalculate paddocks
    if (regenPaddocks) this.setPaddocks({ data: null });
    //update the slider
    this.getRidges.next(this.ridges);
    // update the url query string
    await this.queryParamsSet('ridges-on', this.ridges.on);
    await this.queryParamsSet('ridges-val', this.ridges.value);
  };
  getRidges = new Subject<any>();

  /*
   *  Paddocks:   props, get, set
   **/
  private paddocks: PaddocksLayer = {
    on: false,
    data: undefined,
  };
  setPaddocks = (paddocks: PaddocksLayer) => {
    // console.log('setPaddocks()');
    const assignPaddockColors = () => {
      let COLORS = [];
      if (this.paddocks.data && this.paddocks.data.areas) {
        for (let i = 0; i < this.paddocks.data.areas.length; i++) {
          COLORS.push(
            '#' +
              (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)
          );
        }
      }
      this.paddocks.colors = COLORS;
    };
    // update the layer state
    if (paddocks.on !== undefined) this.paddocks.on = paddocks.on;
    if (paddocks.data === null) this.paddocks.data = undefined;
    if (paddocks.data) this.paddocks.data = paddocks.data;
    if (this.paddocks?.colors?.length !== this.paddocks?.data?.areas?.length)
      assignPaddockColors();
    if (this.paddocks.on && !this.paddocks.data) {
      this.httpDefault({ paddocks: 'placeholder' }, {}, {}).subscribe(
        (result) => {
          this.setPaddocks({ data: result.paddocks });
        }
      );
    }
    // Make the colours display
    this.getPaddocks.next(this.paddocks);
    // update the url query string
    this.queryParamsSet('paddocks-on', this.paddocks.on);
  };
  getPaddocks = new Subject<any>();

  /*
   *  HTTP API:
   *    Default
   **/

  // used by flask to consider what layers require re-calculation
  prevParams = {
    coords: undefined,
    trenchesEven: undefined,
    trenchesStep: undefined,
    gullies: undefined,
    ridges: undefined,
  };

  httpDefault(params: any, layers: any, prevParams: any): Observable<any> {
    if (debug) console.log('httpDefault()');
    if (debug) console.log(params);
    if (debug) console.log(prevParams);

    // This creates a url requesting just a single layer and provides the relevant params
    let url = `${this.api}?`;
    if (params.coords) {
      url += `coords=${params.coords}`;

      //Testing the backend can handle multiple layers at once
      // url += `&aerial=${1}`;
      // url += `&cadastre=${1}`;
      // url += `&trenchesEven=${4}`;
      // url += `&trenchesStep=${4}`;
      // url += `&gullies=${99}`;
      // url += `&ridges=${99}`;
      // url += `&paddocks=${1}`;
    }

    if (params.aerial) {
      params = { aerial: 1 };
      let shape = [this.dem.data.length, this.dem.data[0].length];
      prevParams = { coords: this.prevParams.coords, shape: shape };
      url += `aerial=${params.aerial}`;
    }

    if (params.cadastre) {
      params = { cadastre: 1 };
      let shape = [this.dem.data.length, this.dem.data[0].length];
      prevParams = { coords: this.prevParams.coords, shape: shape };
      url += `cadastre=${params.cadastre}`;
    }

    if (params.trenchesEven) {
      params = { trenchesEven: this.trenchesEven.value };
      layers = { dem: this.dem.data };
      url += `trenchesEven=${params.trenchesEven}`;
    }

    if (params.trenchesStep) {
      params = { trenchesStep: this.trenchesStep.value };
      layers = { dem: this.dem.data };
      url += `trenchesStep=${params.trenchesStep}`;
    }

    if (params.gullies) {
      params = { gullies: this.gullies.value };
      layers = { dem: this.dem.data };
      prevParams = { ridges: this.ridges.value };
      url += `gullies=${params.gullies}`;
    }

    if (params.ridges) {
      params = { ridges: this.ridges.value };
      layers = { dem: this.dem.data };
      prevParams = { gullies: this.gullies.value };
      url += `ridges=${params.ridges}`;
    }

    if (params.paddocks) {
      params = { paddocks: 'paddocks' };
      layers = {
        dem: this.dem.data,
        gullies: this.gullies.value,
        ridges: this.ridges.value,
        trenchesEven: this.trenchesEven.value,
        trenchesStep: this.trenchesStep.value,
      };
      prevParams = {
        gullies: this.gullies.value,
        ridges: this.ridges.value,
        trenchesEven: this.trenchesEven.value,
        trenchesStep: this.trenchesStep.value,
      };
      url += `paddocks=${params.paddocks}`;
    }

    if (debug) console.log(`${url}`);
    return this.http
      .post<any>(
        url,
        { layers: layers, prevParams: prevParams },
        this.httpOptions
      )
      .pipe(
        // return this.http.post<any>(url, layers, this.httpOptions).pipe(
        tap((result: any) => {
          if (debug) console.log(result);
        }),
        catchError(this.handleError<any>('httpDefault'))
      );
  }

  /*
   *  HTTP API:
   *    Single Layer
   **/

  // httpSingleLayer(): Observable<any> {
  //   if (debug) console.log("httpSingleLayer()");

  //   let url = `${this.api}?coords=${this.dem.value}`;
  //   url += `&trenches_even=${this.trenchesEven.value}`;
  //   url += `&trenches_step=${this.trenchesStep.value}`;
  //   url += `&gullies_pysheds=${this.gullies.value}`
  //   url += `&ridges_pysheds=${this.ridges.value}`
  //   if (debug) console.log(`${url}`);

  //   if (debug) console.log(this.prevParams);

  //   return this.http.post<any>(
  //     url,
  //     // provide layers that have already been calculated
  //     // maybe bug on backend as the layers are not being recalculated even when param and prevParam are different.
  //     // potentially fix by separating out each layer into a dedicated http call as should be done anyway.
  //     {
  //       "layers": {},
  //       // "layers": {
  //       //   DEM: this.dem.data,
  //       //   aerial: this.aerial.data,
  //       //   cadastre: this.cadastre.data,
  //       //   trenches_even: this.trenchesEven.data,
  //       //   trenches_step: this.trenchesStep.data,
  //       //   gullies_pysheds: this.gullies.data,
  //       //   ridges_pysheds: this.ridges.data
  //       // },
  //       "prevParams": this.prevParams
  //     },
  //     this.httpOptions).pipe(
  //     tap((result: any) => {
  //       if (debug) console.log(result);
  //     }),
  //     catchError(this.handleError<any>('httpSingleLayer'))
  //   )
  // }

  queryParamsGet = () => {
    if (debug) console.log('queryParamsGet()');
    this.route.queryParamMap.subscribe((queryParamMap) => {
      if (JSON.parse(queryParamMap.get('aerial-on')) !== null)
        this.aerial.on = JSON.parse(queryParamMap.get('aerial-on'));
      if (JSON.parse(queryParamMap.get('cadastre-on')) !== null)
        this.cadastre.on = JSON.parse(queryParamMap.get('cadastre-on'));
      if (JSON.parse(queryParamMap.get('trenches-even-on')) !== null)
        this.trenchesEven.on = JSON.parse(
          queryParamMap.get('trenches-even-on')
        );
      if (JSON.parse(queryParamMap.get('trenches-even-val')) !== null)
        this.trenchesEven.value = JSON.parse(
          queryParamMap.get('trenches-even-val')
        );
      if (JSON.parse(queryParamMap.get('trenches-step-on')) !== null)
        this.trenchesStep.on = JSON.parse(
          queryParamMap.get('trenches-step-on')
        );
      if (JSON.parse(queryParamMap.get('trenches-step-val')) !== null)
        this.trenchesStep.value = JSON.parse(
          queryParamMap.get('trenches-step-val')
        );
      if (JSON.parse(queryParamMap.get('ridges-on')) !== null)
        this.ridges.on = JSON.parse(queryParamMap.get('ridges-on'));
      if (JSON.parse(queryParamMap.get('ridges-val')) !== null)
        this.ridges.value = JSON.parse(queryParamMap.get('ridges-val'));
      if (JSON.parse(queryParamMap.get('gullies-on')) !== null)
        this.gullies.on = JSON.parse(queryParamMap.get('gullies-on'));
      if (JSON.parse(queryParamMap.get('gullies-val')) !== null)
        this.gullies.value = JSON.parse(queryParamMap.get('gullies-val'));
      if (JSON.parse(queryParamMap.get('paddocks-on')) !== null)
        this.paddocks.on = JSON.parse(queryParamMap.get('paddocks-on'));

      // checks to avoid double calling the api
      let coords = queryParamMap.get('coords');
      if (coords && !this.dem.value) {
        this.setDem({ value: coords, loading: true });
      }
    });
  };

  queryParamsSet(k, v) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { [k]: v },
      queryParamsHandling: 'merge',
    });
  }

  httpRequestExport(): Observable<any> {
    if (debug) console.log('httpRequestExport()');

    let url = `${this.api}/export?coords=${this.dem.value}`;
    url += `&trenches_even=${this.trenchesEven.value}`;
    url += `&trenches_step=${this.trenchesStep.value}`;
    url += `&gullies_pysheds=${this.gullies.value}`;
    url += `&ridges_pysheds=${this.ridges.value}`;
    if (debug) console.log(`${url}`);

    if (debug) console.log(this.prevParams);

    return this.http
      .post<any>(
        url,
        // provide layers that have already been calculated
        // maybe bug on backend as the layers are not being recalculated even when param and prevParam are different.
        // potentially fix by separating out each layer into a dedicated http call as should be done anyway.
        {
          //"layers": {},
          layers: {
            DEM: this.dem.data,
            aerial: this.aerial.data,
            cadastre: this.cadastre.data,
            trenches_even: this.trenchesEven.data,
            trenches_step: this.trenchesStep.data,
            gullies_pysheds: this.gullies.data,
            ridges_pysheds: this.ridges.data,
            paddocks: this.paddocks.data,
          },
          prevParams: this.prevParams,
        },
        this.httpOptions
      )
      .pipe(
        tap((result: any) => {
          if (debug) console.log("Export returned: " + result);
        }),
        catchError(this.handleError<any>('httpRequestExport'))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a ModelService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`ModelService: ${message}`);
  }
}
