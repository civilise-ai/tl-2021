import copy

def pad_with_ridges(grid):
    new_grid = []
    dummy_row = []
    # Top and bottom rows
    for i in range(len(grid[0])+2):
        dummy_row.append(-1)
    new_grid.append(dummy_row)
    for row in grid:
        new_row = []
        new_row.append(-1)
        for cell in row:
            new_row.append(cell)
        new_row.append(-1)
        new_grid.append(new_row)
    new_grid.append(dummy_row)
    return new_grid

def remove_padding(grid):
    # grid[1:-1, 1:-1, 1:-1]
    new_grid = []
    for i in range(1, len(grid)-1):
        row = []
        for j in range(1, len(grid[i])-1):
            row.append(grid[i][j])
        new_grid.append(row)
    return new_grid

def all_adjacent_paddocks(grid,i,j):
    paddocks = set()
    if i > 0:
        if grid[i-1][j] > -1:
            paddocks.add(grid[i-1][j])
    if i < len(grid)-1:
        if grid[i+1][j] > -1:
            paddocks.add(grid[i+1][j])
    if j > 0:
        if grid[i][j-1] > -1:
            paddocks.add(grid[i][j-1])
    if j < len(grid[i])-1:
        if grid[i][j+1] > -1:
            paddocks.add(grid[i][j+1])
    return paddocks

def calculate_paddock_stats(grid):
    working_grid = pad_with_ridges(grid)
    paddock_stats = dict()
    num_paddocks = 0    # np.max(grid)
    for i in range(len(working_grid)):
        for j in range(len(working_grid[i])):
            # cell refers to the paddock_id
            cell = working_grid[i][j]
            if cell > num_paddocks:
                num_paddocks = cell
            if cell > -1:
                if cell in paddock_stats:
                    paddock_stats[cell]["area"] = paddock_stats[cell]["area"] + 1
                else:
                    paddock_stats[cell] = dict()
                    paddock_stats[cell]["area"] = 1
                    paddock_stats[cell]["perimeter"] = 0
            else:
                adjacent_paddocks = all_adjacent_paddocks(working_grid,i,j)
                if len(adjacent_paddocks)>0:
                    for paddock in adjacent_paddocks:
                        if paddock in paddock_stats:
                            # Some paddock cells may be used for the perimeter multiple times, because we need to account for each edge of the cell that's on the perimeter
                            paddock_stats[paddock]["perimeter"] = paddock_stats[paddock]["perimeter"] + 1
                        else:
                            paddock_stats[paddock] = dict()
                            paddock_stats[paddock]["area"] = 0
                            paddock_stats[paddock]["perimeter"] = 1
    paddock_stats["num_paddocks"] = num_paddocks + 1
    return paddock_stats

def identify_paddocks(grid):
    working_grid = []
    # np.where(grid == 0, -1000, grid)
    for i in range(len(grid)):
        row = []
        for j in range(len(grid[i])):
            row.append(int(grid[i][j]))
            if row[j] == 0:
                row[j] = -1000
        working_grid.append(row)
    working_grid = pad_with_ridges(working_grid)
    current_paddock = 0
    for i in range(len(working_grid)):
        for j in range(len(working_grid[i])):
            if working_grid[i][j] == -1000:
                working_grid = fill_area(working_grid,i, j, current_paddock)
                current_paddock = current_paddock + 1
    working_grid = remove_padding(working_grid)
    return working_grid

def fill_area(grid, x, y, paddock):
    new_grid = grid.copy()
    cells_visited = set()
    cells_to_visit = set()
    cells_to_visit.add((x,y))
    while len(cells_to_visit) > 0:
        cell = cells_to_visit.pop()
        i = cell[0]
        j = cell[1]
        if i > -1 and i < len(grid):
            if j > -1 and j < len(grid[i]):
                if new_grid[i][j] == -1000:
                    new_grid[i][j] = paddock
                    if (i-1,j) not in cells_visited:
                        cells_to_visit.add((i-1,j))
                    if (i+1,j) not in cells_visited:
                        cells_to_visit.add((i+1,j))
                    if (i,j-1) not in cells_visited:
                        cells_to_visit.add((i,j-1))
                    if (i,j+1) not in cells_visited:
                        cells_to_visit.add((i,j+1))
        cells_visited.add(cell)
    return new_grid

def parse_paddocks_input(input):
    ridges = input["ridges"]
    gullies = input["gullies"]
    trenches = input["trenches"]
    overlay = []
    # ridges | gullies | trenches
    for i in range(len(ridges)):
        row = []
        for j in range(len(ridges[0])):
            if ridges[i][j] or gullies[i][j] or trenches[i][j]:
                row.append(-1)
            else:
                row.append(0)
        overlay.append(row)
    return overlay

def clean_fence_lines(overlay):
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):
            if overlay[i][j] == -1:
                adjacent = all_adjacent_paddocks(overlay,i,j)
                if len(adjacent) == 1:
                    overlay[i][j] = adjacent.pop()
    return overlay

def paddocks(frontend_input):
    output = {}
    # print(len(frontend_input['ridges']))
    # print(len(frontend_input['gullies']))
    # print(len(frontend_input['trenches']))
    overlay = parse_paddocks_input(frontend_input)
    overlay = identify_paddocks(overlay)
    overlay = clean_fence_lines(overlay)
    output["overlay"] = overlay
    paddock_stats = calculate_paddock_stats(overlay)
    areas = []
    perimeters = []
    for i in range(0, paddock_stats["num_paddocks"]):
        areas.append(paddock_stats[i]["area"])
        perimeters.append(paddock_stats[i]["perimeter"])
    output["areas"] = areas
    output["perimeters"] = perimeters
    return output

def combine_min_paddocks(paddocks, min_size):
    smallPaddocksExist = True
    workingPaddocks = copy.deepcopy(paddocks)
    while smallPaddocksExist:
        paddockToCombine = -1
        currentPaddock = 0
        while (paddockToCombine == -1) and (currentPaddock < len(workingPaddocks["areas"])):
            if workingPaddocks["areas"][currentPaddock] < min_size:
                paddockToCombine = currentPaddock
                workingPaddocks = merge_paddocks(workingPaddocks,currentPaddock)
            else:
                currentPaddock = currentPaddock+1
        if paddockToCombine == -1:
            smallPaddocksExist = False
    return workingPaddocks

def all_neighbour_paddocks(paddocks, paddock_index):
    workingGrid = paddocks["overlay"]
    neighbours = set()
    for i in range(len(workingGrid)):
        for j in range(len(workingGrid[0])):
            currentNeighbours = all_adjacent_paddocks(workingGrid, i, j)
            if (len(currentNeighbours) > 1) and (paddock_index in currentNeighbours):
                neighbours = neighbours | currentNeighbours
    neighbours.remove(paddock_index)
    return neighbours

def reset_paddocks(overlay):
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):
            if overlay[i][j]>0:
                overlay[i][j] = 0
    return overlay

def overwrite_paddock(overlay, old_paddock, new_paddock):
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):
            if overlay[i][j]== old_paddock:
                overlay[i][j] = new_paddock
    return overlay

def merge_paddocks(paddocks, paddock_index):
    neighbours = all_neighbour_paddocks(paddocks, paddock_index)
    smallestNeighbour = neighbours.pop()
    smallestSize = paddocks["areas"][smallestNeighbour]
    for paddock in neighbours:
        if paddocks["areas"][paddock] < smallestSize:
            smallestNeighbour = paddock
    paddocks["overlay"] = overwrite_paddock(paddocks["overlay"], paddock_index, smallestNeighbour)
    paddocks["overlay"] = clean_fence_lines(paddocks["overlay"])
    paddocks["overlay"] = reset_paddocks(paddocks["overlay"])
    paddocks["overlay"] = identify_paddocks(paddocks["overlay"])
    paddock_stats = calculate_paddock_stats(paddocks["overlay"])
    areas = []
    perimeters = []
    for i in range(0, paddock_stats["num_paddocks"]):
        areas.append(paddock_stats[i]["area"])
        perimeters.append(paddock_stats[i]["perimeter"])
    paddocks["areas"] = areas
    paddocks["perimeters"] = perimeters
    return paddocks
