from Backend.paddock_plan.paddocks import paddocks, combine_min_paddocks

def test_paddocks():
    print("Testing paddocks and stats function...")
    array1 = [False, False, False, False, False]
    array2 = [False, False, False, False, False]
    array3 = [False, False, False, False, False]
    array4 = [False, False, False, False, False]
    array5 = [False, False, False, False, False]

    empty = []
    empty.append(array1)
    empty.append(array2)
    empty.append(array3)
    empty.append(array4)
    empty.append(array5)

    array1 = [True, False, False, False, False]
    array2 = [False, True, False, False, False]
    array3 = [False, False, True, True, True]
    array4 = [False, False, True, False, False]
    array5 = [True, True, False, False, False]

    ridges = []
    ridges.append(array1)
    ridges.append(array2)
    ridges.append(array3)
    ridges.append(array4)
    ridges.append(array5)

    array1 = [-1, 0, 0, 0, 0]
    array2 = [1, -1, 0, 0, 0]
    array3 = [1, 1, -1, -1, -1]
    array4 = [1, 1, -1, 2, 2]
    array5 = [1, -1, 2, 2, 2]

    expected_overlay = []
    expected_overlay.append(array1)
    expected_overlay.append(array2)
    expected_overlay.append(array3)
    expected_overlay.append(array4)
    expected_overlay.append(array5)

    expected_areas = []
    expected_areas.append(7)
    expected_areas.append(6)
    expected_areas.append(5)

    expected_perimeters = []
    expected_perimeters.append(11)
    expected_perimeters.append(10)
    expected_perimeters.append(9)

    test_input = {}
    test_input["ridges"] = ridges
    test_input["gullies"] = empty
    test_input["trenches"] = empty

    expected_output = {}
    expected_output["overlay"] = expected_overlay
    expected_output["areas"] = expected_areas
    expected_output["perimeters"] = expected_perimeters

    output = paddocks(test_input)
    if output == expected_output:
        print("Test Passed!")
        return True
    else:
        print("Test Failed\n")
        print("Expected Output:")
        print("Overlay")
        for i in range(len(expected_output["overlay"])):
            print(expected_output["overlay"][i])
        print("Areas:")
        print(expected_output["areas"])
        print("Perimeters:")
        print(expected_output["perimeters"])
        print("\nActual Output:")
        for i in range(len(output["overlay"])):
            print(output["overlay"][i])
        print("Areas:")
        print(output["areas"])
        print("Perimeters:")
        print(output["perimeters"])
        return False

#uncomment line to test the paddocks function
test_paddocks()

def test_combine_paddocks():
    print("Testing paddock combine function...")
    array1 = [False, False, False, False, False]
    array2 = [False, False, False, False, False]
    array3 = [False, False, False, False, False]
    array4 = [False, False, False, False, False]
    array5 = [False, False, False, False, False]

    empty = []
    empty.append(array1)
    empty.append(array2)
    empty.append(array3)
    empty.append(array4)
    empty.append(array5)

    array1 = [True, False, False, False, False]
    array2 = [False, True, False, False, False]
    array3 = [False, False, True, True, True]
    array4 = [False, False, True, False, False]
    array5 = [True, True, False, False, False]

    ridges = []
    ridges.append(array1)
    ridges.append(array2)
    ridges.append(array3)
    ridges.append(array4)
    ridges.append(array5)

    array1 = [-1, 0, 0, 0, 0]
    array2 = [1, -1, 0, 0, 0]
    array3 = [1, 1, -1, -1, -1]
    array4 = [1, 1, 1, 1, 1]
    array5 = [1, 1, 1, 1, 1]

    expected_overlay = []
    expected_overlay.append(array1)
    expected_overlay.append(array2)
    expected_overlay.append(array3)
    expected_overlay.append(array4)
    expected_overlay.append(array5)

    expected_areas = []
    expected_areas.append(7)
    expected_areas.append(13)

    expected_perimeters = []
    expected_perimeters.append(11)
    expected_perimeters.append(16)

    test_input = {}
    test_input["ridges"] = ridges
    test_input["gullies"] = empty
    test_input["trenches"] = empty

    expected_output = {}
    expected_output["overlay"] = expected_overlay
    expected_output["areas"] = expected_areas
    expected_output["perimeters"] = expected_perimeters

    output = paddocks(test_input)
    output = combine_min_paddocks(output, 6)
    if output == expected_output:
        print("Test Passed!")
        return True
    else:
        print("Test Failed\n")
        print("Expected Output:")
        print("Overlay")
        for i in range(len(expected_output["overlay"])):
            print(expected_output["overlay"][i])
        print("Areas:")
        print(expected_output["areas"])
        print("Perimeters:")
        print(expected_output["perimeters"])
        print("\nActual Output:")
        for i in range(len(output["overlay"])):
            print(output["overlay"][i])
        print("Areas:")
        print(output["areas"])
        print("Perimeters:")
        print(output["perimeters"])
        return False

test_combine_paddocks()
