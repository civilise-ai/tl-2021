import numpy as np

def paddocks_to_polygons(paddocks):
    workingOverlay = cleanFenceLines(paddocks["overlay"])
    workingOverlay = padWithFence(workingOverlay)
    isolatedFenceLines = []

    # num_paddocks = len(paddocks["areas"]))    # It's convenient to be able to just pass the overlay as input
    num_paddocks = np.max(workingOverlay) + 1

    for i in range(num_paddocks):
        isolatedFenceLines.append(isolateFenceLines(workingOverlay, i))
    boundaryPolygons = []
    for i in range(len(isolatedFenceLines)):
        boundaryPolygons.append(traceBoundary(isolatedFenceLines[i]))
    return boundaryPolygons

def cleanFenceLines(overlay):
    newOverlay = []
    for i in range(len(overlay)):
        column = []
        for j in range(len(overlay[0])):
            if (overlay[i][j] == -1):
                adjacent = adjacentTilesSet(overlay, i, j)
                if (len(adjacent) == 1):
                    column.append(list(adjacent)[0])
                else:
                    column.append(overlay[i][j])
            else:
                column.append(overlay[i][j])
        newOverlay.append(column)
    return newOverlay;

def adjacentTilesSet(overlay, x, y):
    adjacentSet = set();
    if (x > 0):
        if (overlay[x-1][y] > -1):
            adjacentSet.add(overlay[x - 1][y])
    if (x < len(overlay) - 1):
        if (overlay[x + 1][y] > -1):
            adjacentSet.add(overlay[x + 1][y])
    if (y > 0):
        if (overlay[x][y-1] > -1):
            adjacentSet.add(overlay[x][y-1])
    if (y < len(overlay[0]) - 1):
        if (overlay[x][y+1] > -1):
            adjacentSet.add(overlay[x][y+1])
    return adjacentSet

def adjacentTilesArray(overlay, x, y):
    adjacentArray = [0,0,0,0,0,0,0,0];
    if (x == 0):
        adjacentArray[0] = -10
        adjacentArray[1] = -10
        if (y == len(overlay[0])-1):
            adjacentArray[2] = -10
            adjacentArray[3] = -10
        else:
            adjacentArray[2] = overlay[x][y + 1]
            adjacentArray[3] = overlay[x + 1][y + 1]
        adjacentArray[4] = overlay[x + 1][y]
        if(y > 0):
            adjacentArray[5] = overlay[x + 1][y - 1]
            adjacentArray[6] = overlay[x][y - 1]
        else:
            adjacentArray[5] = -10
            adjacentArray[6] = -10
        adjacentArray[7] = -10
    elif (x == len(overlay) - 1):
        adjacentArray[0] = overlay[x - 1][y]
        if (y == len(overlay[0])-1):
            adjacentArray[1] = -10
            adjacentArray[2] = -10
        else:
            adjacentArray[1] = overlay[x - 1][y + 1]
            adjacentArray[2] = overlay[x][y + 1]
        adjacentArray[3] = -10
        adjacentArray[4] = -10
        adjacentArray[5] = -10
        if(y > 0):
            adjacentArray[6] = overlay[x][y - 1]
            adjacentArray[7] = overlay[x - 1][y - 1]
        else:
            adjacentArray[6] = -10
            adjacentArray[7] = -10
    else:
        adjacentArray[0] = overlay[x - 1][y]
        if (y == len(overlay[0])-1):
            adjacentArray[1] = -10
            adjacentArray[2] = -10
            adjacentArray[3] = -10
        else:
            adjacentArray[1] = overlay[x - 1][y + 1]
            adjacentArray[2] = overlay[x][y + 1]
            adjacentArray[3] = overlay[x + 1][y + 1]
        adjacentArray[4] = overlay[x + 1][y]
        if (y>0):
            adjacentArray[5] = overlay[x + 1][y - 1]
            adjacentArray[6] = overlay[x][y - 1]
            adjacentArray[7] = overlay[x - 1][y - 1]
        else:
            adjacentArray[5] = -10
            adjacentArray[6] = -10
            adjacentArray[7] = -10
    return adjacentArray

def isolateFenceLines(overlay, paddockNumber):
    isolatedOverlay = []
    for i in range(len(overlay)):
        col = []
        for j in range(len(overlay[0])):
            if (overlay[i][j] == paddockNumber):
                col.append(paddockNumber)
            else:
                adjacent = adjacentTilesSet(overlay, i, j)
                if paddockNumber in adjacent:
                    col.append(-1)
                else:
                    col.append(-10)
        isolatedOverlay.append(col)
    return isolatedOverlay

def padWithFence(overlay):
    padded = []
    dummyRow = []
    for i in range(len(overlay[0])+2):
      dummyRow.append(-1)

    padded.append(dummyRow)  # Needed this to fix bug with missing paddocks on west edge

    for i in range(1,len(overlay)+1):
        col = []
        col.append(-1)
        for j in range(1, len(overlay[0])+1):
            col.append(overlay[i-1][j-1])
        col.append(-1)
        padded.append(col)
    padded.append(dummyRow.copy())
    return padded;

def traceBoundary(overlay):
    path = []
    found = False
    firstpoint = [0,0]
    for i in range(len(overlay)):
        for j in range(len(overlay[0])):
            if ((found == False) and (overlay[i][j] == -1)):
                firstpoint[0] = i
                firstpoint[1] = j
                found = True
    complete = False
    currentpoint = firstpoint.copy()
    trajectory = 2
    path.append([currentpoint[0],currentpoint[1]])
    max_polygon_size = 10000
    while (complete == False):
        nextPoint = findNextOnPath(overlay,currentpoint, trajectory, path)
        path.append([nextPoint[0][0], nextPoint[0][1]])
        currentpoint[0] = nextPoint[0][0]
        currentpoint[1] = nextPoint[0][1]
        trajectory = nextPoint[1]
        if ((nextPoint[0][0] == path[0][0]) and (nextPoint[0][1] == path[0][1])) or (len(path) > max_polygon_size):
            if (len(path) > max_polygon_size):
                print("Maximum polygon size exceeded")
            complete = True
    path.reverse()
    return path

def findNextOnPath(overlay, point, trajectory, prevPath):
    newtrajectory = trajectory - 2
    if (newtrajectory < 0):
        newtrajectory = newtrajectory + 8
    foundNext = False;
    adjacentArray = adjacentTilesArray(overlay,point[0],point[1])
    while (foundNext == False):
        if (adjacentArray[newtrajectory] == -1 ):
            foundNext = True
        if (foundNext == False):
            newtrajectory = newtrajectory + 1;
        if (newtrajectory > 7):
            newtrajectory = newtrajectory - 8
    newPoint = pointFromTrajectory(newtrajectory, point)
    return [newPoint, newtrajectory]

def pointFromTrajectory(trajectory, point):
    newPoint = [0,0]
    if trajectory == 0: # Left
        newPoint[0] = point[0] - 1;
        newPoint[1] = point[1];
    if trajectory == 1: # Up Left
        newPoint[0] = point[0] - 1;
        newPoint[1] = point[1] + 1;
    if trajectory == 2: # Up
        newPoint[0] = point[0];
        newPoint[1] = point[1] + 1;
    if trajectory == 3: # Up Right
        newPoint[0] = point[0] + 1;
        newPoint[1] = point[1] + 1;
    if trajectory == 4: # Right
        newPoint[0] = point[0] + 1;
        newPoint[1] = point[1];
    if trajectory == 5: # Down Right
        newPoint[0] = point[0] + 1;
        newPoint[1] = point[1] - 1;
    if trajectory == 6: # Down
        newPoint[0] = point[0];
        newPoint[1] = point[1] - 1;
    if trajectory == 7: # Down Left
        newPoint[0] = point[0] - 1;
        newPoint[1] = point[1] - 1;
    return newPoint