import numpy as np
from Backend.trench_plan.Extract_Contours import extract_contours

def trench_plan(Z, num_trenches):
    """Evenly distributes trenches on contours"""
    Z = np.array(np.round(Z), dtype=int)
    minZ = int(np.min(Z))
    maxZ = int(np.max(Z))
    spacing = (maxZ-minZ)//num_trenches
    contours = range(minZ + spacing//2, maxZ, spacing)
    dict_Contours = extract_contours(Z)
    grid = dict_to_grid(Z, dict_Contours, contours)
    return grid

def dict_to_grid(Z, dict_Contours, contours):
    Binary_Grid = np.zeros_like(Z)
    for height in contours:
        height_Contours = dict_Contours[str(height)]
        for height_Contour in height_Contours:
            id_x = height_Contour[:,0].astype(np.uint64)
            id_y = height_Contour[:,1].astype(np.uint64)
            Binary_Grid[id_x, id_y] = True
    return Binary_Grid

# trench cost reference: https://www.airtasker.com/landscaping/trench-digging/price/how-much-does-trench-cost/
def trench_costs(trenches, cost_per_meter=30, contour_res=10):
    trench_length = np.count_nonzero(trenches)
    if contour_res == 1:
        cost = trench_length * cost_per_meter
    elif contour_res == 2:
        cost = trench_length * cost_per_meter * 2
    else:
        cost = trench_length * cost_per_meter * 10
    return trench_length*contour_res, cost