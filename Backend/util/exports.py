from Backend.ridge_and_gully.group_expansion import group_cells
from Backend.paddock_plan.paddock_vector import paddocks_to_polygons, traceBoundary

import numpy as np
import rasterio
from rasterio.transform import Affine
import geopandas as gp
from shapely import geometry

# Rasterio documentation is here: https://rasterio.readthedocs.io/en/latest/quickstart.html#creating-data
def raster_export(bottom_left, layer, output_filename):
    """Export boolean layer with coordinates
    params:
        DEM - Digital elevation model
        bottom_left - an arbitrary corner
        layer - any boolean layer
    return:
        a geojson something
    """
    # The affine transformation is how we geolocate the raster
    # See this stack overflow post for more info on the affine transformations: https://stackoverflow.com/questions/10667834/trying-to-understand-the-affine-transform
    cell_width = 10
    cell_height = 10
    transform = Affine(cell_width, 0, bottom_left[0],
                       0, cell_height, bottom_left[1])

    dtype = rasterio.dtypes.get_minimum_dtype(layer)
    Z = np.array(layer, dtype=dtype)
    Z = Z[::-1]     # I don't know why the dem needs to flipped vertically, it just does.
    new_dataset = rasterio.open(
        output_filename,
        'w',
        driver='GTiff',
        height=Z.shape[0],
        width=Z.shape[1],
        count=1,
        dtype=Z.dtype,
        crs='epsg:28355',
        transform=transform,
    )
    new_dataset.write(Z, 1)     # Just the 1 band of information
    new_dataset.close()
    print(f"Exported {output_filename}")
    # I've been visualising the output Raster (and Vector) in QGIS to check it looks right


def layers_to_linestrings(overlay):
    """This is a replica of the paddocks_to_polygons function but for the other layers"""
    # This grouping function was designed for the ridge connections, but seems useful here
    groups = group_cells(overlay)
    ordered_boundaries = []
    for i, group in enumerate(groups.values()):
        isolated_group = np.zeros(overlay.shape)

        # Convert the group to a 2d array to match the paddock_to_vector script
        indexes = [k for k in group.keys()]
        xs = [index[0] for index in indexes]
        ys = [index[1] for index in indexes]
        isolated_group[xs, ys] = -1

        # Add a buffer of zeros around the array
        buffered_group = np.pad(isolated_group, 1, 'constant', constant_values=(0))

        # Use the traceboundary function to get the boundary of the group.
        ordered_boundary = traceBoundary(buffered_group)
        ordered_boundaries.append(ordered_boundary)

    return ordered_boundaries


def vector_export(bottom_left, layer, output_filename=None, polygons=False):
    """Convert the ridge, gully or trench overlays to a vector format"""
    # No idea why the overlay needs to be rotated and flipped. But it's needed for correct geolocation
    layer = np.flipud(layer)
    layer = np.fliplr(layer)
    layer = np.rot90(layer)

    if polygons:
        # This is for the paddocks (represented by polygons)
        paddock_lists = paddocks_to_polygons({'overlay':layer})
        list_of_shapes = [geometry.Polygon(p) for p in paddock_lists]
    else:
        # This is for the ridges, gullies and trenches (represented by linestrings)
        ordered_boundaries = layers_to_linestrings(layer)
        list_of_shapes = [geometry.LineString(p) for p in ordered_boundaries]

    multishape_gp = gp.GeoDataFrame(list_of_shapes, columns=["geometry"])
    multishape_gp.index.name = "id"
    multishape_gp = multishape_gp.set_crs(crs='epsg:28355')

    # Shapely has a different ordering for the affine values compared to rasterio: https://shapely.readthedocs.io/en/stable/manual.html#shapely.affinity.affine_transform
    affine = [10, 0, 0, 10,  bottom_left[0], bottom_left[1]]
    multishape_gp = multishape_gp.affine_transform(affine)
    multishape_gp = multishape_gp.to_crs(epsg=4326)

    if output_filename:
        multishape_gp.to_file(output_filename, driver='GeoJSON')
        print(f"Exported {output_filename}")
    else:
        return multishape_gp.to_json()
