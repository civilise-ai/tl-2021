import sys, os

sys.path.insert(1, os.getcwd())
from Backend.queries.api_querying import *

ACT_Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?format=jpg&bbox="

# Note: The NSW database contains elevation data inside the ACT too. It just doesn't contain ACT cadastre data.
NSW_10m_contour_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?outFields=elevation&geometry="


def generate_dem_one_point_gp(lat, long, cell_size=10):
    """Generate a rectangular dem for the cadastre at this lat,lon
    params:
        lat - latitude. e.g. -35.28878  (coordinate reference system is EPSG:4326, the one use by google maps)
        lon - longitude. e.g. 149.099330
        cell_size - the dimensions of each cell in meters. Usually 10mx10m
    return:
        dem - a 2d array of floats showing the height at each cell
    """
    isACT = check_if_ACT(lat, long)
    prim_bound, other_bound, _ = generate_cadastre_lat_lon(lat, long, isACT=isACT)
    bound = other_bound if isACT else prim_bound
    dem, bottom_left = generate_DEM_gp(bound, NSW_10m_contour_url, cell_size)
    return dem, bottom_left

def generate_dem_two_points_gp(c_1, c_2, cell_size=10):
    """Generate a rectangular dem within two arbitrary coordinates
    params:
        c_1 - coordinate 1. e.g. [-35.297579, 149.003879] (coordinate reference system is EPSG:4326, the one use by google maps)
        c_2 - coordinate 2. e.g. [-35.280223, 149.018751]
        cell_size - the dimensions of each cell in meters. Usually 10mx10m
    return:
        dem - a 2d array of floats showing the height at each cell
    """
    bound = generate_rect_bounds_gp(c_1, c_2, isACT=False)
    dem, bottom_left = generate_DEM_gp(bound, NSW_10m_contour_url, cell_size)
    return dem, bottom_left

def generate_cadastre_one_point(lat, long, shape, cell_size=10):
    """Find the property boundary from a single coordinate
    params:
        lat - latitude. e.g. -35.28878  (coordinate reference system is EPSG:4326, the one use by google maps)
        lon - longitude. e.g. 149.099330
        shape - the output shape for the mask (should match the dem)
        cell_size - the dimensions of each cell in meters. Usually 10mx10m
    return:
        mask - a 2d array of floats showing the height at each cell
        pts - a 2d array of ints showing which cell belongs to which property
    Notes: The dem_shape overrides the cell_size
           pts.shape may not match the mask.shape
    """
    isACT = check_if_ACT(lat, long)
    box_bound = generate_box_points_gp(lat, long, 0, isACT)
    bound, sec_bound, gs = generate_cadastre(box_bound, isACT)
    mask, pts = extract_points(gs, bound, shape, cell_size)
    return mask

def generate_cadastre_two_points(coord1, coord2, shape, cell_size=10):
    """Find all property boundaries between two coordinates
    params:
        c_1 - coordinate 1. e.g. [-35.297579, 149.003879] (coordinate reference system is EPSG:4326, the one use by google maps)
        c_2 - coordinate 2. e.g. [-35.280223, 149.018751]
        shape - the output shape for the mask (should match the dem)
        cell_size - the dimensions of each cell in meters. Usually 10mx10m
    return:
        mask - a 2d array of floats showing the height at each cell
        pts - a 2d array of ints showing which cell belongs to which property
    Notes: The dem_shape overrides the cell_size
           pts.shape may not match the mask.shape
    """
    isACT = check_if_ACT(coord1[0], coord1[1])
    box_bound = generate_rect_bounds_gp(coord1, coord2, isACT)
    bound, sec_bound, gs = generate_cadastre(box_bound, isACT)
    mask, pts = extract_points(gs, box_bound, shape, cell_size)
    return mask

def RGB_aerial_one_point_gp(lat, long, shape):
    """Fetch satellite imagery for the cadastre at this lat,lon
    params:
        lat - latitude. e.g. -35.28878  (coordinate reference system is EPSG:4326, the one use by google maps)
        lon - longitude. e.g. 149.099330
        shape - the output shape for the mask (should match the dem)
    return:
        rgb_img - a 2d array of rgb lists e.g.[255,255,255]
    """
    isACT = check_if_ACT(lat, long)
    prim_bound, sec_bound, cad_ds = generate_cadastre_lat_lon(lat, long, isACT)
    if isACT:
        image_url = ACT_image_url
    else:
        image_url = NSW_image_url
    img = generate_image_gp(prim_bound, image_url, shape[1], shape[0])
    rgb_img = np.array(img)
    return rgb_img

def RGB_aerial_two_points_gp(c_1, c_2, shape):
    """Fetch satellite imagery within two arbitrary coordinates
    params:
        c_1 - coordinate 1. e.g. [-35.297579, 149.003879] (coordinate reference system is EPSG:4326, the one use by google maps)
        c_2 - coordinate 2. e.g. [-35.280223, 149.018751]
        shape - the output shape for the mask (should match the dem)
    return:
        rgb_img - a 2d array of rgb lists e.g.[255,255,255]
    """
    isACT = check_if_ACT(c_1[0], c_1[1])
    bound = generate_rect_bounds_gp(c_1, c_2, isACT)
    if isACT:
        image_url = ACT_image_url
    else:
        image_url = NSW_image_url
    img = generate_image_gp(bound, image_url, shape[1], shape[0])
    rgb_img = np.array(img)
    return rgb_img
