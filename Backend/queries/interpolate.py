from scipy.interpolate import griddata
import numpy as np
import time

def generate_grid(points, width, height, corners=None):
    """Convert a list of arbitrary (x,y,z) points to a 2d array of interpolated z points
        params:
          points = list of [x,y,z]
          corners = {min_x: _, min_y: _ , max_x: _, max_y: _}"""
    start = time.time()
    if len(points) < 4:
        return []
    z = points[:, 2]

    # Define the grid coordinates. Specifying corners allows for a buffer so we get better accuracy on the edges.
    if corners:
        start_point = [corners["min_x"], corners["min_y"]]
        end_point = [corners["max_x"], corners["max_y"]]
    else:
        start_point = np.min(points, axis=0)
        end_point = np.max(points, axis=0)

    # Use meshgrid to generate a grid map, flip y because the latitude
    X, Y = np.meshgrid(np.linspace(start_point[0], end_point[0], int(width)),
                       np.linspace(end_point[1], start_point[1], int(height)))

    # Use griddata to interpolate unstructured D-D Data
    cubic = griddata(points[:, :2], z, (X, Y), method='cubic')

    # TODO: Should add verbose as an argument so we don't always print this stuff.
    # if np.count_nonzero(np.isnan(cubic)):
    #     print(f"Number of uninterpolated points: {np.count_nonzero(np.isnan(cubic))}")

    # replace nan points (those outside the convex hull) with nearest value
    nearest = griddata(points[:, 0:2], z, (X, Y), method='nearest')
    assert nearest.dtype == float
    Z = np.vectorize(lambda c, n: c if ~np.isnan(c) else n)(cubic, nearest)

    # print(f"Took {time.time()-start}s to complete interpolation")
    return Z