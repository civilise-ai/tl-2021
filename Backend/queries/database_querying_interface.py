from Backend.queries.database_querying import get_points, tablify_points
from Backend.queries.interpolate import generate_grid
from Backend.queries.api_querying import generate_cadastre_lat_lon, check_if_ACT
import numpy as np

# Don't need to specify the cell size, because the database has been pre-interpolated with a size of 10mx10m. Might want multiple databases, each with a different size though?
def query_interpolated_one_point(lat, long, connection):
    """Generate a rectangular dem for the cadastre at this lat,lon.
    params:
        lat - latitude. e.g. -35.28878  (coordinate reference system is EPSG:4326, the one use by google maps)
        lon - longitude. e.g. 149.099330
        cell_size - the dimensions of each cell in meters. Usually 10mx10m
    return:
        dem - a 2d array of floats showing the height at each cell
    """
    isACT = check_if_ACT(lat, long)
    prim_bound, other_bound, _ = generate_cadastre_lat_lon(lat, long, isACT=isACT)
    bound = prim_bound if isACT else other_bound
    corners = {
        "min_x": bound.x[0],
        "min_y": bound.y[0],
        "max_x": bound.x[1],
        "max_y": bound.y[1],
    }
    cursor = connection.cursor()
    points = get_points(cursor, corners, table="interpolated", corners_epsg=28355, table_epsg=28355, sorted=True)
    if len(points) == 0:
        return [], None
    dem = tablify_points(points)
    dem = np.rot90(dem)
    bottom_left = (min(points[:,0]), min(points[:,1]))
    return dem, bottom_left

def query_interpolated_two_points(point1, point2, connection):
    """Generate a rectangular dem within two arbitrary coordinates
    params:
        c_1 - coordinate 1. e.g. [-35.297579, 149.003879] (coordinate reference system is EPSG:4326, the one use by google maps)
        c_2 - coordinate 2. e.g. [-35.280223, 149.018751]
        cell_size - the dimensions of each cell in meters. Usually 10mx10m
    return:
        dem - a 2d array of floats showing the height at each cell
    """
    cursor = connection.cursor()
    corners = {
        "min_x": point1[1],
        "min_y": point1[0],
        "max_x": point2[1],
        "max_y": point2[0],
    }
    points = get_points(cursor, corners, table="interpolated", corners_epsg=4326, table_epsg=28355, sorted=True)
    if len(points) == 0:
        return [], None

    bottom_left = (min(points[:,0]), min(points[:,1]))

    dem = tablify_points(points)
    dem = np.rot90(dem)
    return dem, bottom_left

# This function is for debugging, not needed for the website
def query_uninterpolated_two_points(point1, point2, connection):
    """It's quicker to setup the uninterpolated database"""
    # don't touch
    cursor = connection.cursor()
    cell_size = 10
    corners = {
        "min_x": point1[1],
        "min_y": point1[0],
        "max_x": point2[1],
        "max_y": point2[0],
    }
    print("point1",point1)
    points = get_points(cursor, corners, table="uninterpolated", corners_epsg=4326, table_epsg=28355, sorted=True)
    print("after get points")
    width = (max(points[:,0]) - min(points[:,0]))/cell_size
    height = (max(points[:,1]) - min(points[:,1]))/cell_size
    print("before grid")
    dem = generate_grid(points, width, height)
    print("after grid")
    return dem