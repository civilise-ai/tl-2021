import numpy as np
from copy import copy

adjacencies = np.array([(0, -1), (1, -1), (1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1)])
direct_adjacencies = np.array([(0, -1),  (1, 0),  (0, 1),  (-1, 0)])

def group_expansion(gullies, ridges):
    """Find the shortest path between ridges that does not intersect any gullies"""
    ridge_groups = group_cells(ridges)
    ridges = extend_groups(ridge_groups, gullies)
    return ridges

def inside_grid(coord, shape):
    """Checks a coordinate is within the dimensions of the grid"""
    return coord[0] >= 0 and coord[1] >= 0 and coord[0] < shape[0] and coord[1] < shape[1]

def add_buffer(bool_array):
    """Add a buffer around every cell"""
    xs, ys = np.where(bool_array)
    coords = [(x,y) for x, y in zip(xs, ys)]
    for coord in coords:
        neighbours = adjacencies + coord
        neighbours = [tuple(n) for n in neighbours if inside_grid(tuple(n), bool_array.shape)]
        xs = [n[0] for n in neighbours]
        ys = [n[1] for n in neighbours]
        bool_array[xs, ys] = True
    return bool_array

def extend_groups(ridge_groups, gullies):
    """Repeatedly add layers around groups and combine the groups with the shortest path when they run into each other"""
    ridges = extract_cells(ridge_groups, gullies.shape, use_group_ids=False)
    # The buffer ensures that ridges can't reach across diagonal gaps inside the gullies
    buffered_gullies = add_buffer(copy(gullies))
    used = ridges | buffered_gullies
    has_changed = True
    while len(ridge_groups) > 1 and has_changed:
        print(f"Number of groups: {len(ridge_groups)}")
        found_another_group = False
        has_changed = False
        for index, group in ridge_groups.items():
            max_steps = max(len(g) for g in group.values())
            starting_keys = [g for g in group if len(group[g]) == max_steps]
            for coord in starting_keys:
                neighbours = direct_adjacencies + coord
                neighbours = [tuple(n) for n in neighbours]
                for neighbour in neighbours:
                    if not inside_grid(neighbour, buffered_gullies.shape) or neighbour in group.keys() or buffered_gullies[neighbour]:
                        continue
                    has_changed = True
                    if not used[neighbour]:
                        # Add this cell to the group
                        used[neighbour] = True
                        group[neighbour] = group[coord] + [coord]
                    else:
                        # Add this cell and its backtrack cells to the ridges array
                        used[coord] = True
                        ridges[coord] = True
                        for backtrack_coord in group[coord]:
                            ridges[backtrack_coord] = True
                        # Join the two groups together
                        for other_index, other_group in ridge_groups.items():
                            if neighbour in other_group:
                                # Add the neighbour and its backtrack cells to the ridge array
                                ridges[neighbour] = True
                                for backtrack_coord in other_group[neighbour]:
                                    ridges[backtrack_coord] = True
                                combined_group = {**group, **other_group}
                                del ridge_groups[index]
                                del ridge_groups[other_index]
                                ridge_groups[index] = combined_group
                                break
                        found_another_group = True
                        break
                # Once we've joined two groups together, need to restart the searching from scratch (since the groups have changed)
                if found_another_group:
                    break
            if found_another_group:
                break
    return ridges

def extract_cells(groups, shape, use_group_ids=True):
    """Converts a list of cell dictionaries to a 2d array"""
    extracted = np.zeros(shape, dtype=int)
    for group in groups.keys():
        coords = groups[group].keys()
        xs = [coord[0] for coord in coords]
        ys = [coord[1] for coord in coords]
        if use_group_ids:
            extracted[xs, ys] = group
        else:
            extracted[xs, ys] = 1
    return extracted

def group_cells(bool_array):
    """Group the cells. Each group is a dictionary with the cell coord being the key and the value being a list of cells to get there from the nearest ridge (initially empty)"""
    xs, ys = np.where(bool_array)
    coords = [(x,y) for x, y in zip(xs,ys)]
    groups = dict()
    for coord in coords:
        # Find all the neighbours of the coord
        neighbours = adjacencies + coord
        neighbours = [tuple(n) for n in neighbours]
        assigned_groups = set()
        for neighbour in neighbours:
            for group in groups.keys():
                if neighbour in groups[group].keys():
                    # A new cell may be attached to two different groups
                    assigned_groups.add(group)
        assigned_groups = list(assigned_groups)
        # Create a new group
        if len(assigned_groups) == 0:
            if len(groups) == 0:
                new_group = 0
            else:
                new_group = max(groups.keys()) + 1
            groups[new_group] = {coord:[]}
        # Simply add this cell to the other group
        if len(assigned_groups) == 1:
            groups[assigned_groups[0]][coord] = []
        # Combine the groups together
        if len(assigned_groups) > 1:
            combined_group = {coord:[]}
            assigned_groups = sorted(assigned_groups, reverse=True)
            for group in assigned_groups:
                combined_group = {**combined_group, **groups[group]}
                del groups[group]
            new_group = 0 if len(groups) == 0 else max(groups.keys()) + 1
            groups[new_group] = combined_group
    return groups