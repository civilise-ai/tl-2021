import numpy as np

adjacencies_list = [(0, -1), (1, -1), (1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1)]
adjacencies_array = np.array(adjacencies_list)

def border(coord, shape):
    """Checks a coordinate is within the dimensions of the grid"""
    return coord[0] == 0 or coord[1] == 0 or coord[0] == shape[0] - 1 or coord[1] == shape[1] - 1

def extend_gullies(gullies, acc, endpoints, ridges, early_stopping=True):
    """Use the endpoints to make each gully longer"""
    while len(endpoints) > 0:
        new_endpoints = []
        for gully in endpoints:
            if border(gully, gullies.shape):
                continue
            neighbour_coords = [gully] + adjacencies_array
            # Remove neighbours that are outside of the array
            neighbour_coords = [tuple(coord) for coord in neighbour_coords if coord[0] >= 0 and coord[0] < gullies.shape[0] and coord[1] >= 0 and coord[1] < gullies.shape[1]]
            # If we run into a ridge, then stop extending
            adjacent_ridges = [coord for coord in neighbour_coords if ridges[coord]]
            if len(adjacent_ridges) > 0:
                continue
            # If we already have a neighbour gully with a lower (or equal) accumulation, then skip this cell
            if early_stopping:
                neighbour_gullies_lower = [coord for coord in neighbour_coords if gullies[coord] and acc[coord] <= acc[gully]]
                if len(neighbour_gullies_lower) > 0:
                    continue

            # Find the non-gully neighbour with the highest accumulation
            neighbour_non_gullies = [coord for coord in neighbour_coords if not gullies[coord]]
            if len(neighbour_non_gullies) > 0:
                neighbour_accs = [acc[coord] for coord in neighbour_non_gullies]
                new_gully = neighbour_non_gullies[np.argmax(neighbour_accs)]
                gullies[new_gully] = True
                new_endpoints.append(new_gully)
        endpoints = new_endpoints
    return gullies


def find_endpoints(gullies, acc, upper=True):
    """Find gully cells that don't have any neighbouring gully cells with a lower (or higher) accumulation"""
    endpoints = []
    for i in range(gullies.shape[0]):
        for j in range(gullies.shape[1]):
            if gullies[i][j]:
                neighbour_coords = [i,j] + adjacencies_array
                # Remove neighbours that are outside of the array
                neighbour_coords = [tuple(coord) for coord in neighbour_coords if coord[0] >= 0 and coord[0] < gullies.shape[0] and coord[1] >= 0 and coord[1] < gullies.shape[1]]
                # Find any neighbours with a higher accumulation (or elevation) than the current cell
                if upper:
                    neighbour_gullies = [coord for coord in neighbour_coords if gullies[coord] and acc[i,j] > acc[coord]]
                else:
                    neighbour_gullies = [coord for coord in neighbour_coords if gullies[coord] and acc[i,j] < acc[coord]]
                if len(neighbour_gullies) > 0:
                    continue
                # Only keep the cells without any gully neighbours with a lower accumulation
                endpoints.append((i,j))
    return endpoints
