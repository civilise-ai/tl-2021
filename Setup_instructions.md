## Setup Instructions

#

### Cloning the repo

- To clone the repo:  
   `git clone https://gitlab.com/civilise-ai/tl-2021.git`

#

### Docker
1. Install [Docker](https://docs.docker.com/get-docker/)
2. Navigate to the tl-2021 repo (on bodhi this is currently under)
   - `cd /home/chris/tl-2021`
3. Launch the frontend
   - `sudo docker-compose up nginx`
4. Launch the backend  
   - `sudo docker-compose up backend`  
   - ...
5. Done. If you close the terminal tab, the docker containers will keep running. To stop them, do `sudo docker-compose down`. Other tips:
   - When developing locally, do `sudo docker-compose up frontend` instead of `nginx`. 
   - To update the production version: 
     1.  `git pull` 
     2.  `sudo docker-compose build`
     3.  `sudo docker-compose up nginx` & `sudo docker-compose up backend` as per normal.

#

### (Optional) Conda environment

You can also use a conda environment if you so desire.

You can [download conda here](https://docs.conda.io/en/latest/miniconda.html), and here's a nice [conda cheatsheet](https://conda.io/projects/conda/en/latest/_downloads/843d9e0198f2a193a3484886fa28163c/conda-cheatsheet.pdf). You can check if you have conda with `conda --version`.

- To create your environment:  
   `conda env create -f enjin_env.yml`
- To use your environment:  
   `conda activate enjin`

You can run the backend with `python Frontend/app.py`, and the frontend with `ng serve` (you will need to install angular too)  

#

### (Optional) Python virtual environment

You can also use a [python virtual environment](https://docs.python.org/3/tutorial/venv.html). This is what we're currently using for the preprocessing.

If you don't already have python, you can find the [latest version of python here](https://www.python.org/downloads/). Our code should support any version 3.7+.

- To create your environment:  
  `python3 -m venv enjin`
- To install the dependencies:  
   `source enjin/bin/activate`  
   `pip install -r requirements.txt`

You should now be able to run the backend with `python3 Frontend/app.py`

#

### IDE

You can use [Pycharm](https://www.jetbrains.com/pycharm/download/#section=mac) and as an ANU student, you can get [Pycharm Professional for free](https://www.jetbrains.com/community/education/#students). Or you can use [VSCode](https://code.visualstudio.com/), or [Spyder](https://www.spyder-ide.org/), or whatever else you prefer.

- For VScode, you can follow these instructions to point to the correct environment: https://code.visualstudio.com/docs/python/environments
