#! /bin/bash
FILE="./docker-compose.repl.yml"
PROJECT="tl-2021-repl"

function up(){
    docker-compose -f $FILE -p $PROJECT down
    docker container prune -f
    docker-compose -f $FILE -p $PROJECT up -d --build
}

function down(){
    docker-compose -f $FILE -p $PROJECT down

}
function logs(){
    docker-compose -f $FILE -p $PROJECT logs -f
}

# shellcheck disable=SC2294
eval "$@"