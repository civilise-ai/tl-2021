import matplotlib.pyplot as plt
import numpy as np

def show_contour_overlay(dem, overlay, colour='Red'):
    """Plot an image of the contours and the overlay layer"""
    plt.imshow(dem, alpha=0)
    plt.contour(dem, levels=50, alpha=0.5)
    y, x = np.where(overlay)
    plt.scatter(x, y, marker='.', linewidths=0.01, c=colour)
    plt.show()

def show_contour_overlays(dem, overlays, colours, title=""):
    """Plot an image of the contours and the overlay layer"""
    plt.imshow(dem, alpha=0)
    plt.contour(dem, levels=50, alpha=0.5)
    for overlay, colour in zip(overlays, colours):
        y, x = np.where(overlay)
        plt.scatter(x, y, marker='.', linewidths=0.01, c=colour)
    plt.title(title)
    plt.show()