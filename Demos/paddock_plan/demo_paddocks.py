
import numpy as np
from Backend.trench_plan.equal_area_trenches import equal_area_trench_plan
from Backend.ridge_and_gully.pysheds_accumulation import gullies_and_ridges
from Backend.paddock_plan.paddocks import paddocks
from Backend.util.file_manager import find_data
import matplotlib.pyplot as plt
import plotly.graph_objects as go


# Mulligan's flat
print("Mulligan's flat property")
Z = np.loadtxt(find_data() + "/mulligans_flat.txt")
gullies, ridges = gullies_and_ridges(Z, gully_threshold=99)
trenches = equal_area_trench_plan(Z, num_trenches=5)

frontend_input = {"ridges":ridges, "gullies":gullies, "trenches":trenches}
p = paddocks(frontend_input)

plt.imshow(p["overlay"])
plt.show()

fig = go.Figure(data=[go.Surface(z=Z[::-1], surfacecolor=p["overlay"], showscale=True)])
fig.show()

print(f"Perimeters: {p['perimeters']}")
print(f"Areas: {p['areas']}")
print()
