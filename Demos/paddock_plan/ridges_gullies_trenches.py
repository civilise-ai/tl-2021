import sys, os
sys.path.insert(1, os.getcwd())

import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go

from Backend.trench_plan.evenly_spaced_trenches import trench_plan as evenly_spaced_trench_plan
from Backend.ridge_and_gully.pysheds_accumulation import gullies_and_ridges


if __name__ == "__main__":
    from Backend.util.file_manager import find_data
    Z = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))

    # Sliders
    num_trenches = 10   # Ideal range: 1-10 ish. May want a higher value for finding paddocks, but lower for actual trenches.
    acc_threshold = 98  # Ideal range: 98-99.5 ish (it's a percentile)

    # The main functions
    t = evenly_spaced_trench_plan(Z, num_trenches=num_trenches)
    g, r = gullies_and_ridges(Z, gully_threshold=acc_threshold)

    # 2D matplotlib visualisation
    plt.imshow(Z)
    plt.title(f"trenches: {num_trenches} ,threshold: {acc_threshold}")
    y, x = np.where(t == 1)
    plt.scatter(x, y, marker='.', linewidths=.1, c='green')
    y, x = np.where(g == 1)
    plt.scatter(x, y, marker='.', linewidths=.1, c='blue')
    y, x = np.where(r == 1)
    plt.scatter(x, y, marker='.', linewidths=.1, c='red')
    plt.show()


    # 3D plotly visualisation
    fig = go.Figure(data=[go.Surface(z=Z, colorscale='Viridis', showscale=True)])
    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))
    fig.add_scatter3d(x=np.where(t == True)[1],
                      y=np.where(t == True)[0],
                      z=Z[t == True] + 3,
                      mode='markers',
                      marker=dict(
                          size=1,
                          color='green',
                          opacity=.8
                      ))
    fig.add_scatter3d(x=np.where(g == True)[1],
                      y=np.where(g == True)[0],
                      z=Z[g == True] + 3,
                      mode='markers',
                      marker=dict(
                          size=1,
                          color='blue',
                          opacity=.8
                      ))
    fig.add_scatter3d(x=np.where(r == True)[1],
                      y=np.where(r == True)[0],
                      z=Z[r == True] + 3,
                      mode='markers',
                      marker=dict(
                          size=1,
                          color='red',
                          opacity=.8
                      ))
    fig.update_layout(title=f"trenches: {num_trenches} ,threshold: {acc_threshold}", autosize=True,
                      margin=dict(l=65, r=50, b=65, t=90))
    fig.show()


