## Trench Plan

### Introduction
The trench plan is designed to find the optimal contours to dig trenches along.

### What is a trench?
A trench (can be known as a 'contour') is a long cutting in the landscape roughly 30cm-1m deep that follows a contour - doesn't go up or down.

### Why trenches are important in our project?
Trenches have a number of purposes:
* They slow down water flow, reducing erosion
* They provide places for water to pool giving it time to seep into the ground
* They distribute water across the land instead of the water just funneling into gullies like normal

### Versions
We currently have 2 different versions of the trench plan. They are:
- Evenly spaced:
    - Distribute trenches with even spacing along the creek with the highest accumulation
    - This is best for terrain that has a fairly constant gradient
- Step terrain:
    - Place trenches on the edge plateaus so that the land below the trench is quite steep, but the land above is fairly flat.
    - This exaggerates the effect that the flat land already has on slowing the water flow
    - This is best for undulating terrain.

### Features
* *Input*
  * Grid: Array (mxn)
  * Number of contours: Integer
* *Output*
  * Grid: Boolean array showing which cells should be trenches


### Future
- Ensure each trench has a reasonably sized catchment above and below
- Constant area trenches
- Filter out everything outside the property boundary (assuming the property is not rectangular).
  