# Queries
These scripts contain the code necessary to setup and use our own database that we host, as well as code for querying NSW government database. 

## Loading the pre-made database
1. brew install postgresql and postgis.
1.5 Start postgresql locally
    - `pg_ctl -D /usr/local/var/postgres start`
2. create a superuser & database
    - `sudo -u postgres psql`
    - `CREATE user me with encrypted password 'me';`
    - `ALTER USER me WITH SUPERUSER;`
    - `CREATE database act;`
    - `exit`
3. Enable PostGIS
    - `psql -U me -d act`
    - `CREATE EXTENSION postgis;`
    - `exit`
4. Load the database
    - Can download berridale (2GB) from here: https://drive.google.com/file/d/1rLJhQV98mpcqTx0lBM9EwiiwlbQwNx3c/view?usp=sharing
        - Note: This file is slightly out of date, there was a stitching bug when it was generated. Need to make a new small example file.
    - psql -U me -d act < (filename.sql)
5. Confirm it loaded correctly by running the Backend.queries.database_querying_interface on a coordinate in berridale
    (TODO: construct a proper test case to confirm it loaded correctly)