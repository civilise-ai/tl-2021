import numpy as np
from Backend.queries.interpolate import generate_grid
from Demos.util.debugging import Progress_estimator
from Backend.queries.database_querying import get_points, connect_to_database

# You need to follow the instructions in Database_creation to setup the database before running this script!

def get_boundary(cursor):
    query = "select min(st_x(point)), min(st_y(point)), max(st_x(point)), max(st_y(point)) from uninterpolated"
    cursor.execute(query)
    fetched = cursor.fetchall()[0]
    full_boundary = {"min_x":fetched[0], "min_y":fetched[1], "max_x":fetched[2], "max_y":fetched[3]}
    return full_boundary

def attach_xy(grid, corners, cell_size, boundary):
    """Attach x and y values to each z value in the grid
        params:
            grid = 2d numpy array of ints
            corners = {min_x: _, min_y: _ , max_x: _, max_y: _}
        return:
            list of [x,y,z] based off the grid"""
    if len(grid) == 0:
        return []
    interpolated = []
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            # Important to note that I'm recording the coordinate in the center of the cell. (not bottom left)
            x = corners["min_x"] + cell_size * i + cell_size/2
            y = corners["min_y"] + cell_size * j + cell_size/2
            z = grid[i][j]
            # Don't include points that are outside the uninterpolated table
            if x > boundary["min_x"] and x < boundary["max_x"] and y > boundary["min_y"] and y < boundary["max_y"]:
                interpolated.append((x,y,z))
    return interpolated

def insert_points(connection, cursor, points, table="interpolated", epsg=28355):
    """Insert the points into the table"""
    values = ', '.join(f"(ST_SetSRID(ST_POINT({p[0]}, {p[1]}), {epsg}), {p[2]})" for p in points)
    query = f"INSERT INTO {table} VALUES {values} ON CONFLICT(point) DO NOTHING"
    cursor.execute(query)
    connection.commit()
    # print(f"Time taken to insert {len(points)} points: {time.time() - start}")

def full_interpolation():
    """Interpolate everything in the uninterpolated table"""
    connection = connect_to_database("me", "me", "nsw")
    cursor = connection.cursor()

    boundary = get_boundary(cursor)
    cell_size = 10
    buffer_size = 100
    batch_size = 1000
    width = height = batch_size // cell_size
    num_batches_x = (int(boundary["max_x"] - boundary["min_x"]) // batch_size) + 1
    num_batches_y = (int(boundary["max_y"] - boundary["min_y"]) // batch_size) + 1

    # progress_estimator = Progress_estimator(num_batches_x * num_batches_y, "Interpolation loop")
    for i in range(num_batches_x):
        for j in range(num_batches_y):
            corners = {"min_x": boundary["min_x"] + batch_size * i,
                       "min_y": boundary["min_y"] + batch_size * j,
                       "max_x": boundary["min_x"] + batch_size * i + batch_size - cell_size,
                       "max_y": boundary["min_y"] + batch_size * j + batch_size - cell_size}
            # This rounds the coords to the nearest 10.
            # But ideally we want to round to the nearest "cell_size". This will currently have bugs on overlapping regions with cell_sizes other than 10.
            corners = {"min_x": round(corners["min_x"], -1),
                       "min_y": round(corners["min_y"], -1),
                       "max_x": round(corners["max_x"], -1),
                       "max_y": round(corners["max_y"], -1)}
            corners_with_buffer = {"min_x": corners["min_x"] - buffer_size,
                                   "min_y": corners["min_y"] - buffer_size,
                                   "max_x": corners["max_x"] + buffer_size,
                                   "max_y": corners["max_y"] + buffer_size}
            points = get_points(cursor, corners_with_buffer)
            grid = generate_grid(points, width, height, corners)
            if len(grid) > 0:
                grid = np.rot90(grid, k=3)  # Not sure about this yet
                interpolated = attach_xy(grid, corners, cell_size, boundary)
                insert_points(connection, cursor, interpolated)
            # progress_estimator()

if __name__ == '__main__':
    full_interpolation()