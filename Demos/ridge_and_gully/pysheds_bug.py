import os
import matplotlib.pyplot as plt
import numpy as np
from Backend.util.file_manager import find_data
from Backend.ridge_and_gully.pysheds_accumulation import gullies_and_ridges, acc_cutoff
from Demos.util.visualisation import show_contour_overlays

if __name__ == "__main__":
    dem = np.loadtxt(os.path.join(find_data(), "black_mountain.txt"))
    # dem = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))
    # dem = np.loadtxt(os.path.join(find_data(), "araluen.txt"))
    threshold = 98

    ridges, ridge_acc = acc_cutoff(-dem, threshold, fill_depressions=True)
    gullies, gullies_acc = acc_cutoff(dem, threshold, fill_depressions=True)


    title = "Ridges Bug"
    show_contour_overlays(dem, [ridges, gullies], ["red", "blue"], title)