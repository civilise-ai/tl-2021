import argparse

from funky.src.auto_commit.auto_commit import auto_commit
from funky.src.app import App

def main():
  parser = argparse.ArgumentParser(description='Application Description')
  for α in [
    ('-a', '--auto-commit', 'Auto-commit if tests pass', 'store_true')
  ]:
    parser.add_argument(α[0], α[1], help=α[2], action=α[3])
  args = parser.parse_args()

  if args.auto_commit:
    auto_commit()
  else:
    app = App()
    app.run()

if __name__ == '__main__':
  main()
  print('fin')
