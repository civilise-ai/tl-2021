#! /bin/bash
FILE="./docker-compose.prod.yml"
PROJECT="tl-2021-prod"

function up(){
  sudo docker-compose -f $FILE -p $PROJECT down
  sudo docker container prune -f
  sudo docker-compose -f $FILE -p $PROJECT up -d --build
}

function down(){
  sudo docker-compose -f $FILE -p $PROJECT down
}

function logs(){
  sudo docker-compose -f $FILE -p $PROJECT logs -f
}

# shellcheck disable=SC2294
eval "$@"