FROM python:3.8-slim-buster

WORKDIR /app

ENV VIRTUAL_ENV=enjin
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV FLASK_APP=Frontend/app.py
ENV FLASK_ENV=development
ENV FLASK_RUN_HOST=0.0.0.0

# Install dependencies:
COPY setup.py .
COPY requirements.txt .
COPY enjin_env.yml .
RUN pip install -r requirements.txt
# COPY enjin enjin
# RUN python3 -m venv enjin
RUN python3 -m venv $VIRTUAL_ENV
# RUN source enjin/bin/activate

COPY . .

VOLUME ["/app/Backend"]
VOLUME ["/app/Frontend"]

EXPOSE 5000

# ENTRYPOINT FLASK_APP=Frontend/app.py flask run --host=0.0.0.0
ENTRYPOINT flask run
