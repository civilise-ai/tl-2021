from Backend.backendManager import parse_coord
import unittest


coord='-34.737893, 150.530024'
coords='-34.737893, 150.530024; -34.737893, 150.530024'

class MyTestCase(unittest.TestCase):
    def test_parsecoord(self):
        single_coord=parse_coord(coord)
        self.assertEqual(single_coord,  ['-34.737893', ' 150.530024'])
        corner_coords=parse_coord(coords)
        self.assertEqual((corner_coords),[[-34.737893, 150.530024], [-34.737893, 150.530024]])
if __name__ == '__main__':
    unittest.main()
