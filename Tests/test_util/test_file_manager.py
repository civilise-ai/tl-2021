from Backend.util.file_manager import find_root, find_data
import os
import unittest

class Test_Trenches(unittest.TestCase):

    def test_find_root(self):
        """ensure the root path contains the project name"""
        assert 'tl-2021' in find_root()

    def test_find_test_data(self):
        """ensure the test_data path contains the project name/Tests/Data"""
        assert os.path.join('tl-2021/Data') in find_data()

if __name__ == '__main__':
    unittest.main()