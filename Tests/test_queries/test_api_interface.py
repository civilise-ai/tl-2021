import unittest
from Backend.queries.api_querying_interface import *


class Test_API(unittest.TestCase):

    coord = -35.28878, 149.01080  # Spring Valley
    coords = [-35.297579, 149.003879], [-35.280223, 149.018751]  # Spring Valley

    def test_generate_dem_one_point_gp(self):
        dem, bottom_left = generate_dem_one_point_gp(self.coord[0], self.coord[1], cell_size=10)
        assert dem.shape == (269, 147)  # Should be same with ACT or NSW contours (There used to be an incorrect EPSG conversion)

    def test_generate_dem_two_points_gp(self):
        dem, bottom_left = generate_dem_two_points_gp(self.coords[0], self.coords[1], cell_size=10)
        assert dem.shape == (189, 139)

    def test_generate_cadastre_one_point(self):
        cadastre = generate_cadastre_one_point(self.coord[0], self.coord[1], [269, 147], cell_size=10)
        assert cadastre.shape == (269, 147)

    def test_generate_cadastre_two_points(self):
        cadastre = generate_cadastre_two_points(self.coords[0], self.coords[1], (236, 165), cell_size=10)
        assert cadastre.shape == (236, 165)

    def test_RGB_aerial_one_point_gp(self):
        img = RGB_aerial_one_point_gp(self.coord[0], self.coord[1], [1024,1024])
        assert img.shape == (1024,1024, 3)

    def test_RGB_aerial_two_points_gp(self):
        img = RGB_aerial_two_points_gp(self.coords[0], self.coords[1], [1024, 1024])
        assert img.shape == (1024, 1024, 3)


if __name__ == '__main__':
    unittest.main()
