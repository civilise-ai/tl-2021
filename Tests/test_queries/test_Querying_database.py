
from Backend.queries.database_querying import *
import os
import numpy as np
from Backend.util.file_manager import find_data
import unittest


class MyTestCase(unittest.TestCase):
    dem_spring = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))
    def test_connection(self):
        # connection = psycopg2.connect(user="me",
        #                               password="me",
        #                               host="civilise.czksfc9vxm4y.ap-southeast-2.rds.amazonaws.com",
        #                               port=5432,
        #                               database="nsw")
        user = "me"
        password = "me"
        host = "civilise.czksfc9vxm4y.ap-southeast-2.rds.amazonaws.com"
        port = 5432
        database = "nsw"
        connection=connect_to_database(user,password,database,host,port)
        self.assertIsNotNone(connection)
    def test_tablifypoints(self):
        dem=self.dem_spring
        points=tablify_points(dem)
        print(points)# [[566.12492736]]


if __name__ == '__main__':
    unittest.main()
