import unittest
import numpy as np
from Backend.queries.interpolate import generate_grid
from Backend.util.file_manager import find_data
import os



class Testgridding(unittest.TestCase):
    def test_generate_grid_null(self):
        generatingGrid = generate_grid([], 147, 269)
        self.assertEqual(len(generatingGrid), 0)

    def test_generate_grid(self):
        gridPoints = os.path.join(find_data(), "test_griddata")
        points = np.loadtxt(gridPoints)

        generatingGrid = generate_grid(points, 147, 269)
        # assert generatingGrid.shape == (269, 147)
        self.assertEqual(generatingGrid.shape, (269, 147))


if __name__ == '__main__':
    unittest.main()
