import unittest

from Backend.queries.database_querying_interface import *
import psycopg2

from os import environ

establish_database_connection = lambda: psycopg2.connect(
  user="me",
  password="me",
  host=environ.get('DB_HOST'),
  port="5432",
  database="nsw"
)

class Test_database(unittest.TestCase):
  connection = establish_database_connection()

  coord = -35.28878, 149.01080    # Spring Valley
  coords = [-35.297579, 149.003879], [-35.280223, 149.018751] # Spring Valley

  # coord = -36.361544, 148.806901  # Berridale
  # coords = [[-35.594910, 149.800382],[-35.568433, 149.849032]]    # Araluen


  def test_query_interpolated_one_point(self):
    dem, bottom_left = query_interpolated_one_point(self.coord[0], self.coord[1], self.connection)
    assert dem.shape == (269, 147)

  def test_query_interpolated_two_points(self):
    dem, bottom_left = query_interpolated_two_points(self.coords[0], self.coords[1], self.connection)
    assert dem.shape == (196, 139)


if __name__ == '__main__':
  unittest.main()
