import unittest
from Backend.ridge_and_gully.endpoints import *
from Backend.ridge_and_gully.pysheds_accumulation import water_accumulation
from Backend.util.file_manager import find_data
import os


class MyTestCase(unittest.TestCase):
    dem_spring = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))

    dem = np.array(dem_spring)
    acc = water_accumulation(dem)

    def test_extendgullies(self):
        acc_threshold = np.percentile(self.acc, 98)
        gullies = np.vectorize(lambda a: a > acc_threshold)(self.acc)
        endpoints = find_endpoints(gullies, self.acc)
        ridges = np.vectorize(lambda a: a > 99)(water_accumulation(-self.dem))
        gullies = extend_gullies(gullies, self.acc, endpoints, ridges)
        self.assertEqual(gullies.shape, (269, 147))
        # add assertion here

    def test_findendpoints(self):
        acc_threshold = np.percentile(self.acc, 98)
        gullies = np.vectorize(lambda a: a > acc_threshold)(self.acc)
        endpoints = find_endpoints(gullies, self.acc)
        self.assertEqual(len(endpoints), 18)


if __name__ == '__main__':
    unittest.main()
