import os
import numpy as np
from Backend.util.file_manager import find_data, find_root
from Backend.ridge_and_gully.pysheds_accumulation import *
import unittest


class Test_Ridges(unittest.TestCase):
    dem_spring = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))

    # def test_closed_ridges(self):
    #     example = np.loadtxt(os.path.join(find_data(), "closed_ridges"))
    #     dem = self.dem_spring
    #     calculated = closed_ridges(dem, threshold=99)
    #     assert example.shape == calculated.shape
    #
    # def test_closed_gullies(self):
    #     example = np.loadtxt(os.path.join(find_data(), "closed_gullies"))
    #     dem = self.dem_spring
    #     calculated = closed_gullies(dem, threshold=99)
    #     assert example.shape == calculated.shape
    # def test_ridgecosts(self):
    #     cost1=ridge_costs(10,40,1)
    #     cost2= ridge_costs(10, 40, 2)
    #     self.assertEqual(cost1,(1,40))
    #     self.assertEqual(cost2,(2,80))
    def test_water(self):
        dem_water = water_accumulation(self.dem_spring)
        self.assertEqual(len(dem_water), 269)

    def test_ridgesandgullies(self):
        dem = self.dem_spring
        ridges,gullies=gullies_and_ridges(dem, 98, True)
        assert ridges.shape == (269, 147)
        assert gullies.shape == (269, 147)

    def test_accCutoff(self):
        dem=self.dem_spring
        gullies,acc=acc_cutoff(dem,98,True)
        # Here both have same shape (269,147)
        assert gullies.shape == acc.shape
    def test_demtogrid(self):
        dem=self.dem_spring
        grid_=dem_to_grid(dem)
        assert grid_.shape == (269,147)
if __name__ == '__main__':
    unittest.main()
