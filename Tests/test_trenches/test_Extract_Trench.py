from Backend.trench_plan.Extract_Trench import *
from Backend.util.file_manager import find_data
from Backend.trench_plan.Extract_Contours import extract_contours
import os
import unittest


class MyTestCase(unittest.TestCase):
    def test_extractTrench(self):
        dem = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))    # add assertion here
        dict_Contours = extract_contours(dem)
        extractTrench = extract_trench(dem, dict_Contours, [268,146])
        assert extractTrench.shape == (457, 2)

if __name__ == '__main__':
    unittest.main()
