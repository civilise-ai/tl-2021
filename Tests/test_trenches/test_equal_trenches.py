from Backend.trench_plan.equal_area_trenches import *
from Backend.util.file_manager import find_data
import os
import unittest


class MyTestCase(unittest.TestCase):
    def test_equaltrenchplan(self):
        dem = np.loadtxt(os.path.join(find_data(), "spring_valley.txt"))
        equaltrenches= equal_area_trench_plan(dem,10)
        assert equaltrenches.shape == (269,147)

if __name__ == '__main__':
    unittest.main()
