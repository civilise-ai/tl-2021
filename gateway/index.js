const express = require('express')
const proxy = require('express-http-proxy')
const app = express()
const port = process.env.PORT
const opts = {limit: '1024mb'}

app.all("/api", proxy(process.env.CONTAINER_BE, opts))
app.get('*', proxy(process.env.CONTAINER_FE, opts))
app.listen(port, () => {console.log(`listening @ http://localhost:${port}`)})
