CREATE ROLE replicator WITH REPLICATION PASSWORD 'me' LOGIN;

CREATE table public.naturals (a int);
INSERT INTO public.naturals (a) VALUES (1), (2), (3), (4), (5);

CREATE TABLE bools (a boolean, b text);
INSERT INTO bools VALUES (TRUE, '1==1');
INSERT INTO bools VALUES (FALSE, '1==0');
