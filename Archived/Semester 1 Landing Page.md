# Civilise.AI Techlauncher 2021 Landing Page

## Project Summary
Our project is an online tool to give professional-level land management advice to all rural landowners in the ACT NSW region.

We are achieving this goal by simulating models of entire landscapes, by utilising a combination of machine learning and bespoke algorithmic techniques.

This allows us to optimise groundwater levels, manage erosion, generate efficient paddock plans and tree planting guides tailor made to a specific property.

This will help landowners to better take care of their land to preserve both natural biodiversity and commercial output for generations to come.

You can see our website at http://civilise.ai/

You can see a video showcasing our project [here](https://drive.google.com/file/d/1-TI7CnRn0gJOG6e8-P7SyLzw-zbgB4JH/view?usp=sharing)

## Codebase
We are using gitlab to develop code collaboratively. This semester we ended up rewriting, or at least refactoring, most of the codebase. The code structure is as follows:

### Backend (Python)

#### Trench plan 
* Based on stakeholder feedback, we improved the 'evenly_spaced_trenches' to make the 'equal_area_trenches' which sparked further discussion for a trench plan based on our water and erosion model. 

#### Ridge and gully 
* Through code review and peer programming, we reduced this code from 200 lines to 40, and improved performance by a factor of 5, while achieving the same functionality.

#### Queries
* To improve performance and reliability, we refactored our government databases queries, and redesigned our local database to support spatial indexing.

#### Paddocks 
* Using test driven development, we crafted a fast and scalable function to split the property into paddocks. 

### Tests
The testing directory is new this semester. We have been following test driven development practices to help improve code quality.

### Demos
This is where we demonstrate the functions in the backend, and create visualisations.

### civilise-ng (Angular)
This is the new frontend. We converted the frontend to angular this semester to promote better coding practices. The latest angular code can be seen in the [API branch](https://gitlab.com/civilise-ai/tl-2021/-/tree/api)

### Frontend 
This is the old frontend. We are still using part of this to interface with the backend, but a large part is now obsolete.

## Project Documentation
We are using [google drive](https://drive.google.com/drive/folders/1xJXiuPo0l_YHa-qVCW2JOHOGDgGNUica?usp=sharing) to manage our project documentation. This includes:

Project Definition
- [Statement of Work](https://docs.google.com/document/d/1PGj-1mo_lCIW8GFEE10422AOVZf7kiJw0nAviurSe7I/edit)
- [Retrospective Roadmap](https://docs.google.com/document/d/1VNeECTWnNbNt-K0lyexHgmv4-w7uoKUWCVjeh_Yu9hw/edit?usp=sharing)
- [Team Charter](https://docs.google.com/document/d/1qYylRca5Lp6gHTA58NumpALNG4He_HDpN23nTbUqp6k/edit?usp=sharing)
- [Kanban board](https://gitlab.com/civilise-ai/tl-2021/-/boards/2479436)

Audits
- [Audit 1 slides](https://docs.google.com/presentation/d/1ZkTdzKqEiqwX6akAf5KbLFffKG4oySeLPUd_cVNzGdI/edit?usp=sharing)
- [Audit 2 slides](https://drive.google.com/file/d/1lfK4XRWvtnCCZrcFPBD_6e7wWcZrd8An/view?usp=sharing)
- [Audit 3 slides](https://drive.google.com/file/d/1mEXJatLW5igHxrR4ae4B8yfM7xLVNdDx/view?usp=sharing)

Minutes
- [05-03-2021](https://docs.google.com/document/d/1kKlOoP21T60ocnHBzWuIyEV9joO4FJXdgo3HVoxB_sg/edit?usp=sharing)
- [09-03-2021](https://docs.google.com/document/d/1245IwltajPAyTtxSfSYPd6EGA0dI6SGuGjT2Yg2M1ZE/edit?usp=sharing)
- [10-03-2021](https://docs.google.com/document/d/1GgTkfWGYYGAQqQN875Y5wdH4l5FW2h0s-iWYbCdfAdA/edit?usp=sharing) (Audit 1)
- [12-03-2021](https://docs.google.com/document/d/1sBe1JlIUmjutMM5jsbSe_0cf_qao5rQWmS4CEop493c/edit?usp=sharing)
- [19-03-2021](https://docs.google.com/document/d/1eBnh5pE8lxVUWXGll4IxUTEmGmxH3WNqPw76fbp-K8I/edit?usp=sharing)
- [24-03-2021](https://docs.google.com/document/d/17tMhVYNcJSRm0_qfId-Pu6y263MplyJXUDlWqUxAz_o/edit?usp=sharing)
- [26-03-2021](https://docs.google.com/document/d/1zVRek96k-IAZMSJxxOZdF6vEHEEE1bJCGEKqmfkkXo4/edit?usp=sharing) (Customer meeting)
- [26-03-2021](https://docs.google.com/document/d/1f4h0YaFfAWmZwoBZQm4-0KWsJn51hXP3aRxWOrdOFF0/edit?usp=sharing)
- [29-03-2021](https://docs.google.com/document/d/1wIX9HpcnzD3fq87cBUrWveggyKwCmA4WrBF9YJVqz_U/edit?usp=sharing)
- [30-03-2021](https://docs.google.com/document/d/1MDeD9HFfFLz1nnsYyYUl98gYW95091TekiwXgEiIQVY/edit?usp=sharing)
- [31-03-2021](https://docs.google.com/document/d/1hpwLeDdL8g7LLQgDXc5mzKcNZOJ9cCha3uABDp0zlio/edit?usp=sharing) (Audit 2)  
- [09-04-2021](https://docs.google.com/document/d/1mErMTa-0G_8yamy-OwHAdteEutlqr7uEK-762wdZyVI/edit?usp=sharing)
- [14-04-2021](https://docs.google.com/document/d/1vx9anP_Ek1tlAN6fhTq4ekdR9E6OlKuuiDuf7yPlSvw/edit?usp=sharing)
- [21-04-2021](https://docs.google.com/document/d/12fl_jtKjID6UXer2LNiG0Bzflvu6p7wXtrdqxmi8-Jw/edit?usp=sharing)
- [23-04-2021](https://docs.google.com/document/d/18q7CahOgulq72U1GF6SHmjoy04fKz1jKEW3NxWu9R0U/edit?usp=sharing) (Customer meeting)
- [30-04-2021](https://docs.google.com/document/d/1ZZhV4KEI5mzCkCyGo6gle7KYdgb60qTDZpM-uqfb_xA/edit?usp=sharing)
- [07-05-2021](https://docs.google.com/document/d/1ECbNf5iQ5HWS78y23PYzWY6w-OGiiguYqeNzYWSZauU/edit)

Other documentation
- [Decision Log](https://docs.google.com/spreadsheets/d/1UZZYfxHM9E23oYzX5pNOVE8-pUFiqjfMVPhKNyY9Y-M/edit?usp=sharing)
- [Risk Register](https://docs.google.com/spreadsheets/d/1VWVtTQ3iJ2JKHntTQ2qDvVy8Z_i1DI_9zmC1lyRdnp0/edit?usp=sharing)
- [Diagrams](https://drive.google.com/drive/folders/1bmD9NYkihuGsOXkh6cNUpS0FckOXO4k1?usp=sharing)
- [Code Reviews](https://drive.google.com/drive/folders/1QJIZhzXKrjLN6XapGUNOPgVc14RBshvO?usp=sharing)
- [API Documentation](https://docs.google.com/document/d/1qgIBkTmV1hCt5-lYz9KBV2f-j0gJT0YyPuYUKNYOumI/edit?usp=sharing)
