
////////// CONSTANTS feel free to tweak ////
var aspectRatio = 1.5;  // Aspect ratio of the 3D viewer window
var screenScale = 0.7;  // How big the 3D viewer window should be compared to the screen size
var landscapeHeightSize = 1.1; // Maximum on-screen height of landscape in three.js world units
var landscapeWidthSize = 6.0   // Maximum on-screen width or length of landscape in three.js world units
// TODO: var maximumWidth = 256 // If the mesh is above this number, scale down the entire mesh
////////// END CONSTANTS ///////////////////


var width, length, minimumHeight, maximumHeight, heightScalingFactor, widthScalingFactor;

// Do not call this function externally, use showLandscape() instead.
// Pass this function a 2D array of the height at every grid point in the landscape
// Returns a landscape mesh that can be displayed
var generateLandscape = function(landscapeArray, colourArray){

    // Default landscape
    if (landscapeArray == undefined){
        landscapeArray =[[20, 25, 20, 19],
                         [23, 23, 22, 17],
                         [15, 10, 16, 17],
                         [16, 12, 12, 17],
                         [23, 23, 22, 13]];
    }

    // Gather metadata
    width = landscapeArray.length;
    length = landscapeArray[0].length;

    minimumHeight = 100000;
    maximumHeight = -100000;
    for (x=0;x<width;x++){
        for (z=0;z<length;z++){
            maximumHeight = Math.max(maximumHeight, landscapeArray[x][z]);
            minimumHeight = Math.min(minimumHeight, landscapeArray[x][z]);
        }
    }
    // For flatter properties let's not exagerate the scaling too much
    if (maximumHeight-minimumHeight < 25){
        landscapeHeightSize = 0.5;
        console.log("This is a fairly flat property. Reducing height scale to 0.5");
    }
    heightScalingFactor = landscapeHeightSize/(maximumHeight-minimumHeight);
    widthScalingFactor = landscapeWidthSize/Math.max(width, length);

    // Debug log the metadata
    console.log("width " + width + " length " + length + " HSF " + heightScalingFactor + " WSF " + widthScalingFactor);

    var landscapeGeometry = new THREE.BufferGeometry();

    var indices = [];
    var vertices = [];
    var normals = [];
    var colors = [];

    // generate vertices, normals and color Data for a simple grid geometry
    for (var i=0;i<width;i++){
        for (var j=0;j<length;j++) {

            var z = (i*widthScalingFactor) - (width*widthScalingFactor*0.5);    // fix these
            var x = (j*widthScalingFactor) - (length*widthScalingFactor*0.5);   //
            var y = (landscapeArray[i][j]-minimumHeight)*heightScalingFactor;

            vertices.push(x,y,z);
            normals.push(0,0,1);


            // Add colours to the mesh
            if (colourArray == undefined){
                colors.push(1.0, 0.0, 1.0);
            }else{
                if (i < colourArray.length && j < colourArray[0].length){
                    // Using layer instead of cadastreMap atm.
                    // if (cadastreMap != null){
                    //     if (i < cadastreMap.length){
                    //         if (j < cadastreMap[i].length){
                    //             if (!cadastreMap[i][j]){
                    //                 grey = (colourArray[i][j][0]+colourArray[i][j][1]+colourArray[i][j][2])/3.0;
                    //                 colourArray[i][j][0] += grey*0.3;
                    //                 colourArray[i][j][1] += grey*0.3;
                    //                 colourArray[i][j][2] += grey*0.3;
                    //                 colourArray[i][j][0] *= 0.73;
                    //                 colourArray[i][j][1] *= 0.73;
                    //                 colourArray[i][j][2] *= 0.73;
                    //             }else{
                    //                 colourArray[i][j][0] *= 1.05;
                    //                 colourArray[i][j][1] *= 1.05;
                    //                 colourArray[i][j][2] *= 1.05;
                    //             }
                    //         }
                    //     }
                    // }
                    colors.push(colourArray[i][j][0]/255.0,colourArray[i][j][1]/255.0,colourArray[i][j][2]/255.0);
                }else{
                    colors.push(1.0, 0.0, 1.0);
                }
            }
            

            
        }
    }

    // Generate triagnles
    for (var i=0;i<width-1;i++){
        for (var j=0;j<length-1;j++){

            var a = i*length + j;
            var b = i*length + j + 1;
            var c = (i+1)*length + j;
            var d = (i+1)*length + j + 1;

            // generate two faces (triangles) per iteration
            indices.push(a,b,c);
            indices.push(c,b,d);
        }
    }

    landscapeGeometry.setIndex(indices);
    landscapeGeometry.setAttribute('position',new THREE.Float32BufferAttribute(vertices,3));
    landscapeGeometry.setAttribute('normal',new THREE.Float32BufferAttribute(normals,3));
    landscapeGeometry.setAttribute('color',new THREE.Float32BufferAttribute(colors,3));

    var material = new THREE.MeshStandardMaterial( {
        side: THREE.DoubleSide,
        vertexColors: true
    });

    landscapeGeometry.castShadow = true;
    landscapeGeometry.receiveShadow = true;

    return new THREE.Mesh(landscapeGeometry, material);
}


// Constrain the display to the appdiv on the app page
var drawingSurface = document.getElementById('appcanvas');

// Set up THREE.js scene
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, aspectRatio, 0.1, 1200 );
var renderer = new THREE.WebGLRenderer( { antialias: true, canvas: drawingSurface } );
renderer.setSize( window.innerWidth*screenScale, window.innerWidth*screenScale/aspectRatio );
renderer.setClearColor("#d6efff");
// document.body.appendChild( renderer.domElement );

let materialArray = [];
let texture_sky = new THREE.TextureLoader().load('static/images/sky.png')
let texture_up = new THREE.TextureLoader().load('static/images/top.png')
let texture_dn = new THREE.TextureLoader().load('static/images/bottom.png')
  
materialArray.push(new THREE.MeshBasicMaterial( { map: texture_sky }));
materialArray.push(new THREE.MeshBasicMaterial( { map: texture_sky }));
materialArray.push(new THREE.MeshBasicMaterial( { map: texture_up }));
materialArray.push(new THREE.MeshBasicMaterial( { map: texture_dn }));
materialArray.push(new THREE.MeshBasicMaterial( { map: texture_sky }));
materialArray.push(new THREE.MeshBasicMaterial( { map: texture_sky }));
   
for (let i = 0; i < 6; i++)
  materialArray[i].side = THREE.BackSide;
   
let skyboxGeo = new THREE.BoxGeometry( 1000, 1000, 1000);
let skybox = new THREE.Mesh( skyboxGeo, materialArray );
scene.add( skybox );

// Add a light source
var amblight = new THREE.AmbientLight( 0xffffff );
scene.add(amblight);

// Add our landscape
var landscapeMesh;

// Position the camera in a downward view
camera.position.z = 5;
camera.position.y = 1.75;
camera.rotation.x = -0.3;

// Add the camera orbit controls
var controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.update();

// Let the window be resized live
window.addEventListener( 'resize', () => {
    renderer.setSize( window.innerWidth*screenScale, window.innerWidth*screenScale/aspectRatio );
})

var animate = function () {
    requestAnimationFrame( animate );
    // landscapeMesh.rotation.y -= 0.005;
    controls.update();
    renderer.render( scene, camera );
};

var clone3DArray = function (arr){
    var arr2 = [];
    for (var a=0;a<arr.length;a++){
        var arr2A = [];
        for (var b=0;b<arr[a].length;b++){
            var arr2B = [];
            for (var c=0;c<arr[a][b].length;c++){
                arr2B.push(arr[a][b][c]);
            }
            arr2A.push(arr2B);
        }
        arr2.push(arr2A);
    }
    return arr2;
}


// Continuity variables
var isAnimating = false;
var landscapeArrayCache = undefined;    // Used to remember the landscape so we can add images to it without reloading the array externally
var baseImageCache = undefined;         // Used to remember the initial image pasted on there so we know how to add or remove boolean map overlays
var booleanMaps = new Array(10).fill(undefined);       // Used to store all of the boolean arrays ([n][x][y] of booleans where n is the index of the boolean map) as well as colour, opacity and visibility

//////// EXTERNALLY CALLABLE FUNCTIONS  //////////

/*
 * showLandscape
 * This function takes a 2D array of height values, and displays it as a mesh on the webpage
 * Can be called repeatedly for example as a simulation progresses.
 * 
*/
var showLandscape = function(landscapeArray){
    landscapeArrayCache = landscapeArray;
    scene.remove(landscapeMesh);
    landscapeMesh = generateLandscape(landscapeArray, baseImageCache);
    scene.add(landscapeMesh);

    // Make sure we don't call the animation function more than once
    if (!isAnimating){
        isAnimating = true;
        animate();
    }
}


/*
 * showOverlayImage
 * This function takes a 2D array of colours the same size as the mesh and overlays it onto the existing landscape mesh
 * 
*/
var showBaseImage = function(newColourArray){
    if (landscapeArrayCache == undefined){
        console.error("Overlay Image: Tried to overlay image onto mesh when no mesh was loaded");
        return;
    }

    // We can't add colours after we've added the mesh to our scene, so we have to delete the old one and add a new one with colour from the landscapeArrayCaches
    scene.remove(landscapeMesh);
    landscapeMesh = generateLandscape(landscapeArrayCache, newColourArray);
    scene.add(landscapeMesh);

    // Add this image array to cache so we can revert to it later
    baseImageCache = clone3DArray(newColourArray);
}

/*
 * generateDefaultTexture
 * Generates a default texture image array and applies to the current loaded mesh
 * If an rgb map is given as an argument, this will be mixed in as well
*/
var showDefaultTexture = function(aerial_rgb){
    if (landscapeArrayCache == undefined){
        console.error("showDefaultTexture: Tried to generate default texture for mesh when no mesh was loaded");
        return;
    }

    var defaultColourArray = [];
    for (var i=0;i<width;i++){
        var arr2A = [];
        for (var j=0;j<length;j++){
            var y = (landscapeArrayCache[i][j]-minimumHeight)*heightScalingFactor;  // Convert raw height Data to mesh Data (same equation used in generateLandscape())
            
            // Get heights of surrounding points so we can compare and set it's colour
            var y1, y2, y3, y4 = y;
            if (i < width-1){y1 = (landscapeArrayCache[i+1][j]-minimumHeight)*heightScalingFactor;}
            if (i > 0)      {y2 = (landscapeArrayCache[i-1][j]-minimumHeight)*heightScalingFactor;}
            if (j < length-1){y3 = (landscapeArrayCache[i][j+1]-minimumHeight)*heightScalingFactor;}
            if (j > 0)      {y4 = (landscapeArrayCache[i][j-1]-minimumHeight)*heightScalingFactor;}

            // Set Set how much of each colour cmyb we want based on height differences
            colourC = Math.log(0.8*(y3-y1)/landscapeHeightSize+1.005)*65*128*((width+length)/500.0);

            // Set midpoint (colour of flat region) and clip to 0
            colourC += 137;
            colourC = Math.max(colourC, 0);

            // Clip to 255
            if (colourC > 255){colourC = 255;}

            // Convert to rgb colours that look pretty nice
            colourR = Math.min(colourC*1.0-10,255)*0.95;
            colourG = Math.min((colourC)*1.1,255);
            colourB = Math.min((colourC)*0.9-15,255)*0.9;

            // Mix in aerial imagery if it's provided
            if (aerial_rgb != null){
                if (i < aerial_rgb.length){
                    if (j < aerial_rgb[i].length){
                        //colourR = (colourR + aerial_rgb[i][j][0])/2;
                        //colourG = (colourG + aerial_rgb[i][j][1])/2;
                        //colourB = (colourB + aerial_rgb[i][j][2])/2;
                        colourR = aerial_rgb[i][j][0]*(1.25*colourC+30)/255;
                        colourG = aerial_rgb[i][j][1]*(1.25*colourC+30)/255;
                        colourB = aerial_rgb[i][j][2]*(1.25*colourC+30)/255;

                        // Tint landscape green
                        colourR = colourR*0.95;
                        colourG = colourG*1.15;
                        colourB = colourB*0.85;
                    }
                }
            }

            // Do the black and white thing with the cadastre (This is now done in generate mesh function so it applies to all layers)
            /*if (cadastreMap != null){
                if (i < cadastreMap.length){
                    if (j < cadastreMap[i].length){
                        if (!cadastreMap[i][j]){
                            colourGrey = (colourR + colourG + colourB)/3;
                            colourR = colourGrey;
                            colourG = colourGrey;
                            colourB = colourGrey;
                        }
                    }
                }
            }*/

            arr2A.push([Math.max(0,Math.min(colourR,255)), Math.max(0,Math.min(colourG,255)), Math.max(0,Math.min(colourB,255))]);
        }
        defaultColourArray.push(arr2A);
    }

    // We can't add colours after we've added the mesh to our scene, so we have to delete the old one and add a new one with colour from the landscapeArrayCaches
    scene.remove(landscapeMesh);
    landscapeMesh = generateLandscape(landscapeArrayCache, defaultColourArray);
    scene.add(landscapeMesh);

    // Add this image array to cache so we can revert to it later
    baseImageCache = clone3DArray(defaultColourArray);
}


/*
 * addOverlay()
 * Adds an overlay into the boolean overlays array, along with colour, opacity and visibility
 **/
var addOverlay = function(index, boolArrayy, overlayColourr, opacityy, visiblee){
    if (index > booleanMaps.length){
        console.error("addOverlay: tried to add an overlay at index " + index + " when max booleanmaps is " + booleanMaps.length);
        return;
    }
    booleanMaps[index] = {boolArray:boolArrayy, overlayColour:overlayColourr, opacity:opacityy, visible:visiblee};
}


/*
 * toggleOverlay()
 * This get's called when one of the switches is clicked. It then sets the corrosponding overlay's visibility to tru of false
 **/
// TODO: Figure out what this 'path' is doing. Code breaks without it, but not sure why
var toggleOverlay = function(index, path, layerName, layerValue){

    if (booleanMaps[index] == undefined){
        console.error("toggleOverlay: tried to modify visibility of overlay " + index + " when that index is undefined")
        return
    }
    var checkBox = document.getElementById("switch" + index);
    booleanMaps[index].visible = checkBox.checked;
    console.log ("toggleOverlay: Overlay " + index + " has been set to " + checkBox.checked + ".");

    // If the layer is empty then send a form to generate that layer
    if (booleanMaps[index]["boolArray"] == null){
        xmlHttp = request_layer(path, layerName, layerValue)
        xmlHttp.onload = function(){
            booleanMaps[index]["boolArray"] = JSON.parse(xmlHttp.responseText)
            overlayBooleanMaps();
        }
        xmlHttp.send();
    } else {
        // Refresh the model
        overlayBooleanMaps();
    }
}

/*
 * toggleSatellite()
 * Turns the satellite imagery overlay on and off
 **/
var toggleSatellite = function(){

    var checkBox = document.getElementById("switchSatellite");
    console.log ("toggleSatellite: Satellite overlay has been set to " + checkBox.checked + ".");
    if (!aerialImagery){
        xmlhttp = request_layer(path, "aerial", -1)  // not using the slider yet. Later the 'layer_value' be hopefully be used to choose the image resolution.
        xmlhttp.onload = function(){
            aerialImagery = JSON.parse(xmlhttp.responseText)
            showDefaultTexture(aerialImagery);
            overlayBooleanMaps();
        }
        xmlhttp.send()
    }
    else if (checkBox.checked){
        showDefaultTexture(aerialImagery);
    }else{
        showDefaultTexture(null);
    }
    overlayBooleanMaps();
}

var refresh_toggle = function(id, toggle_state) {
    if (toggle_state) {
        document.getElementById("switch" + id).checked = true
    }
}

var request_layer = function(path, layer_name, layer_value){
    var xmlHttp = new XMLHttpRequest();
    params = layer_name + "=" + layer_value  // not using the slider yet. Later this will hopefully be used to choose the image resolution.
    path = path + "?" + params;
    console.log("Requesting: " + path);
    xmlHttp.open( "POST", path, true);
    return xmlHttp
}


/*
 * overlayBooleanMap
 * This function takes a 2D array of booleans, a colour (as [r, g, b]) and opacity (0.0-1.0) and overlays it onto the image laready on the map
 * 
*/
var overlayBooleanMaps = function(){

    console.log("Overlaying boolean maps")
    if (landscapeArrayCache == undefined){
        console.warn("Overlay Boolean: Tried to overlay image onto mesh when no mesh was loaded");
        return;
    }

    // If there is no base image previously loaded then initialise it to sumthin idk
    if (baseImageCache == undefined){
        console.warn("Overlay Boolean: No base landscape colour is loaded yet. Defaulting to this awful magenta")
        baseImageCache = new Array(width).fill(new Array(length).fill([255,0,255]));
    }

    // Copy our base colour array to a new one that we will apply to the mesh
    var colourArray = clone3DArray(baseImageCache);

    // Mix up our colours (this is pretty epic ngl)
    for (var index=0;index<booleanMaps.length;index++){
        if (booleanMaps[index] != undefined){
            if (booleanMaps[index].visible){
                var boolArray = booleanMaps[index].boolArray;
                var overlayColour = booleanMaps[index].overlayColour;
                var opacity = booleanMaps[index].opacity;

                for (var i=0;i<width-1;i++){
                    for (var j=0;j<length-1;j++){

                        if (i > boolArray.width-1){
                            console.warn("Overlay Boolean X: Boolean map is smaller than landscape mesh: " + i);
                            i = width+1;
                            j = length+1;   // This is how i break loops. I know, shut up
                        } else if (j > boolArray[0].length-1){
                            console.warn("Overlay Boolean Y: Boolean map is smaller than landsacpe mesh: " + j);
                            i = width+1;
                            j = length+1;
                        } else if (boolArray[i][j]){
                            colourArray[i][j][0] = Math.min(255, (overlayColour[0]*opacity + colourArray[i][j][0]*(1.0-opacity)));
                            colourArray[i][j][1] = Math.min(255, (overlayColour[1]*opacity + colourArray[i][j][1]*(1.0-opacity)));
                            colourArray[i][j][2] = Math.min(255, (overlayColour[2]*opacity + colourArray[i][j][2]*(1.0-opacity)));

                        }

                    }
                }
            }
        }
    }

    // We can't add colours after we've added the mesh to our scene, so we have to delete the old one and add a new one with colour from the landscapeArrayCaches
    scene.remove(landscapeMesh);
    landscapeMesh = generateLandscape(landscapeArrayCache, colourArray);
    scene.add(landscapeMesh);
}

// post function taken from https://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit
/**
 * sends a request to the specified url from a form. this will change the window location.
 * @param {string} path the path to send the post request to
 * @param {object} params the paramiters to add to the url
 * @param {string} [method=post] the method to use on the form
 */

function post(path, params, method='post') {

  // The rest of this code assumes you are not using a library.
  // It can be made less wordy if you use one.
  const form = document.createElement('form');
  form.method = method;
  form.action = path;

  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const hiddenField = document.createElement('input');
      hiddenField.type = 'hidden';
      hiddenField.name = key;
      hiddenField.value = params[key];

      form.appendChild(hiddenField);
    }
  }

  document.body.appendChild(form);
  form.submit();
}
