#! /bin/bash
FILE="./docker-compose.dev.yml"
PROJECT="tl-2021-dev"

function up(){
  sudo docker-compose -f $FILE -p $PROJECT down
  sudo docker container prune -f
  sudo docker-compose -f $FILE -p $PROJECT up -d --build
}

function up_database(){
  sudo docker-compose -f $FILE -p $PROJECT down
  sudo docker container prune -f
  sudo docker-compose -f $FILE -p $PROJECT up -d --build database
}

function down(){
  sudo docker-compose -f $FILE -p $PROJECT down

}
function logs(){
  sudo docker-compose -f $FILE -p $PROJECT logs -f
}

# shellcheck disable=SC2294
eval "$@"