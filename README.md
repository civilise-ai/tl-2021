# Civilise.AI Techlauncher 2021 Landing Page

## Project Summary
Our project is an online tool to give professional-level land management advice to all rural landowners in the ACT NSW region.

We are achieving this goal by simulating models of entire landscapes, by utilising a combination of machine learning and bespoke algorithmic techniques.

This allows us to optimise groundwater levels, manage erosion, generate efficient paddock plans and tree planting guides tailor made to a specific property.

This will help landowners to better take care of their land to preserve both natural biodiversity and commercial output for generations to come.

You can see our website at http://civilise.ai/

You can see our showcase video (Semester 2 2021) [here](https://drive.google.com/file/d/1C0zTTl_kn4NaF8zJ29KlcVcLsAsiqXZ7/view?usp=sharing)

## Project Documentation
We are using [google drive](https://drive.google.com/drive/folders/1xJXiuPo0l_YHa-qVCW2JOHOGDgGNUica?usp=sharing) to manage our project documentation. This includes:

Project Plan
- [Statement of work](https://docs.google.com/document/d/15_izBdc9f2UrAEn6n23s06v4BnGd-bdvVxZTUMW7Xbc/edit)
- [Timeline](https://docs.google.com/spreadsheets/d/13FIAxVDaBcb93VszRJtfxF8kEIf5SdISmHwb5p4b9Gw/edit#gid=402919145)
- [Decision log](https://docs.google.com/spreadsheets/d/1s5mye7eJ3HLN75bpub-AeXoaF-fr77e95iIhBO8vesU/edit?usp=sharing)
- [Risk register](https://docs.google.com/spreadsheets/d/13LHvQDJX3tz4-Q887T69jOZv3c1FS08PyMzSovEu6qs/edit?usp=sharing)
- [Reflection Log](https://docs.google.com/spreadsheets/d/1do8nd_hhwc1grf2rAUjYOkM4Xqk4tXyHXz9fj8Q2ujI/edit#gid=0)
- [Problematisation Analysis](https://drive.google.com/drive/folders/1C_AaSYqrg-4BTeKN9YtkzFE6VpdpXt6i)

Audits
- [Audit 1 slides](https://docs.google.com/presentation/d/16SvmbgHM4d7ztHNd9KjGO3aeg2BBCu4u/edit?usp=sharing&ouid=101089072442640272878&rtpof=true&sd=true)
- [Audit 2 slides](https://docs.google.com/presentation/d/1VAqNHLwSYNbJ1LhyVAjOhlGPso_pH9FR/edit?usp=sharing&ouid=101089072442640272878&rtpof=true&sd=true)
- [Audit 3 slides](https://docs.google.com/presentation/d/10zXzyBaWtlqTbm4w1jvO2Zox51MaAEqS/edit?usp=sharing&ouid=101089072442640272878&rtpof=true&sd=true)

Agendas
- [05-08-2021 - Thursday Week 2](https://docs.google.com/document/d/10S24qYEbXE_vvf_--0grmdQ_XHD1bgqCbFBH-fe98UU/edit?usp=sharing)
- [19-08-2021 - Thursday Week 4](https://docs.google.com/document/d/1D-pUfARZQsD1ehcy9gtpvU70vPF9a9ZZGrve9hGcPks/edit?usp=sharing)
- [20-08-2021 - Friday Week 4](https://docs.google.com/document/d/1DHwvHlzvBH2uvDavTqsxqNyUBTyP0jiXShBxjFcWlhA/edit?usp=sharing)
- [21-08-2021 - Saturday Week 4](https://docs.google.com/document/d/1xrlulo8mOHQOfQ6x8nTF9msvr9HIF6UVXyHjmQLIrUE/edit?usp=sharing)
- [24-08-2021 - Tuesday Tutorial Week 5](https://docs.google.com/document/d/1pTT1L_i3URvgqmL39YuqXoOTsyysrzYg1rdYqP7pUFI/edit)
- [26-08-2021 - Thursday Week 5](https://docs.google.com/document/d/1uQuYQ_iSHcgUuNsPZMHND6InUwu4yXxrVIDBANi9vwI/edit)
- [09-09-2021 - Thursday Break 1](https://docs.google.com/document/d/1lUT7EKTeYB1nAmsl0kyuYCSRpRsMM_7hE4iFDUUDfuQ/edit?usp=sharing)
- [16-09-2021 - Thursday Break 2](https://docs.google.com/document/d/1pPA6cUW85Q9H0QDez3paKlMljphOsm7SBNDtDYCyttA/edit?usp=sharing)
- [23-09-2021 - Thursday Week 7](https://docs.google.com/document/d/1REveiAf9sci5nPAQDAVlK6wALz8FWUfwRvkhyCTLVjc/edit?usp=sharing)
- [29-09-2021 - Wednesday Week 8](https://docs.google.com/document/d/1U4s36ujH0EevOUCZRNWcFPep2Z4OffprY7vLDZtJvl0/edit?usp=sharing)
- [30-09-2021 - Thursday Week 8](https://docs.google.com/document/d/1S5-yWetco_zhzN1QdXRkLtAH_HtUlySaNHwuH5ZFH3c/edit?usp=sharing)
- [07-10-2021 - Thursday Week 9](https://docs.google.com/document/d/1VnFxFUoB1pls2aThsBlfljBv17Yln6cRXIg168Mr4m0/edit?usp=sharing)

Minutes
- [29-07-2021 - Thursday Week 1](https://docs.google.com/document/d/1L4PQ26hHO_tlG9IhTkImO5fTV7Z8iUJjVf5aZ8oH9wU/edit?usp=sharing)
- [05-08-2021 - Thursday Week 2](https://docs.google.com/document/d/1lHe6qXSs08_gj5QZzglAIYAGR4vYlZoA67FcoKAHNJU/edit)
- [19-08-2021 - Thursday Week 4](https://docs.google.com/document/d/1Hs6tPawLkFssjbOY34dWn9VfoE_81C-nbJRARZysgkA/edit)
- [20-08-2021 - Friday Week 4](https://docs.google.com/document/d/1VQh-djw0h8Q7xCVMZ0IQ60HOoM-LVe6mlhnfm8AG9I4/edit?usp=sharing) (Customer Meeting)
- [26-08-2021 - Thursday Week 5](https://docs.google.com/document/d/18gHIfWR3bvm4VW5e0T5SUcjVcf_e7tNAN6i-aXtrulI/edit?usp=sharing)
- [09-09-2021 - Thursday Break 1](https://docs.google.com/document/d/1wxdCNqE3LyqfvP5yAqERUy_YJo-u2KdcXJlskiCekDA/edit?usp=sharing)
- [16-09-2021 - Thursday Break 2](https://docs.google.com/document/d/198UIWQMhz9Tfn9jcqWZ-AmLFaO3qHQOJ64UinTP1eAU/edit?usp=sharing)
- [23-09-2021 - Thursday Week 7](https://docs.google.com/document/d/1Q3h4F9hrbxYXwJP03grmM5AZRlE0Hk5uDVO-b173FL4/edit?usp=sharing)
- [29-09-2021 - Wednesday Week 8](https://docs.google.com/document/d/1mveqrUqrwvFPRT1oqWI7TqSUJJz3TQ8rJ-_W2iTlzbo/edit?usp=sharing) (Customer Meeting)
- [30-09-2021 - Thursday Week 8](https://docs.google.com/document/d/1UQrmcCAVlbv6Hj2wuPMDN5VAlfygjxg-hfQCqM_O9Sc/edit?usp=sharing)
- [07-10-2021 - Thursday Week 9](https://docs.google.com/document/d/1jWB-_MhwuK59eDrvVigSULNKCsZ_VceqybMNXZrnlfE/edit?usp=sharing)

Handover Documentation
- [Ridges and Gullies](https://docs.google.com/document/d/1rO3GITV9pvFCUKKH8yV5xrYt0o__MWo5r-y5Gej_690/edit?usp=sharing)
- [Contour Trenching](https://docs.google.com/document/d/1DuUPrynhzIshczGFtuNl902vlbuMZ9uqWCmC7oPs3z0/edit?usp=sharing)
- [Exports](https://docs.google.com/document/d/1O_U6cF0uFNZlZxKsQGbpsqaeXhpwqmIHnvasEtxo1O4/edit?usp=sharing)
- [Queries](https://docs.google.com/document/d/1dBikTRu08oHzAQK8oo8e7Yx1bzjDxr0BZTh2XFCg5hg/edit?usp=sharing)
- [Testing](https://docs.google.com/document/d/10cHcnH3dXJyxP2X6TSAHvCvXxagJTKSv/edit?usp=sharing&ouid=101089072442640272878&rtpof=true&sd=true)

## Outputs Since Audit 2

- [Drag box limit](0eb0d372)
    - This avoids selecting large areas that load indefinitely
    ![](Assets/drag_box_limit.png)

- [Colour selection](0707e838) 
    - So the user can choose the colour of each layer
    ![](Assets/colour_selection.png)

- [New exports](04cd00a9) 
    - Rasters & Vectors for ridges, gullies, trenches & paddocks
    - (not quite connected to frontend)
    ![](Assets/all_exports.png)

- [Paddock merging](ed6a851b)
    - (not quite connected to frontend)

- [Docker nginx](b0550b24)
    - One command to setup the frontend, backend, and nginx for deployment
    ![](Assets/docker_x3.png)

- [Automated Testing & Continuous Integration](4ceb515b)
    - Created a subprocess to run all tests at once
    - Used our client's testing library "pyfunc"
    ![](Assets/tests_passing.png)

- Control flow diagrams for important modules


