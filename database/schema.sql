--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: contours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contours (
    wkb_geometry public.geometry,
    elevation bigint
);


ALTER TABLE public.contours OWNER TO postgres;

--
-- Name: interpolated; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.interpolated (
    point public.geometry NOT NULL,
    z double precision
);


ALTER TABLE public.interpolated OWNER TO postgres;

--
-- Name: new; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.new (
    gid integer NOT NULL,
    classsubty character varying(200),
    featurerel date,
    attributer date,
    capturesou character varying(200),
    capturemet character varying(200),
    planimetri character varying(200),
    verticalac integer,
    elevation bigint,
    sourceprog character varying(200),
    mapsheetna character varying(200),
    classifica character varying(200),
    relevance character varying(200),
    geom public.geometry(MultiLineStringZM,7854)
);


ALTER TABLE public.new OWNER TO postgres;

--
-- Name: new_gid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.new_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.new_gid_seq OWNER TO postgres;

--
-- Name: new_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.new_gid_seq OWNED BY public.new.gid;


--
-- Name: uninterpolated; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.uninterpolated (
    point public.geometry,
    z integer
);


ALTER TABLE public.uninterpolated OWNER TO postgres;

--
-- Name: new gid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.new ALTER COLUMN gid SET DEFAULT nextval('public.new_gid_seq'::regclass);


--
-- Name: interpolated interpolated_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.interpolated
    ADD CONSTRAINT interpolated_pkey PRIMARY KEY (point);


--
-- Name: new new_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.new
    ADD CONSTRAINT new_pkey PRIMARY KEY (gid);


--
-- Name: interpolated_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX interpolated_idx ON public.interpolated USING gist (point);


--
-- Name: new_geom_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_geom_idx ON public.new USING gist (geom);


--
-- PostgreSQL database dump complete
--
